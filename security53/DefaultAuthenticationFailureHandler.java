package com.gitee.dbswitch.admin.security53;

import com.gitee.dbswitch.admin.common.response.Result;
import com.gitee.dbswitch.admin.common.response.ResultCode;
import com.gitee.dbswitch.data.util.JsonUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@Component
@Slf4j
public class DefaultAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler {
    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
        log.info("login in failure : " +  exception.getMessage());

        response.setContentType("application/json;charset=utf-8");
        response.setCharacterEncoding("utf-8");
        PrintWriter writer = response.getWriter();
        String message;
        if (exception instanceof BadCredentialsException) {
            message =  "用户名或密码错误，请重试。";
            writer.write(JsonUtils.toJsonString(Result.failed(ResultCode.ERROR_ACCESS_FORBIDDEN,message)));
        }else{
            writer.write(JsonUtils.toJsonString(Result.failed(ResultCode.ERROR_ACCESS_FORBIDDEN,exception.getMessage())));
        }
        writer.flush();
    }
}


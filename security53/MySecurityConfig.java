package com.gitee.dbswitch.admin.security53;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

//public class MySecurityConfig extends WebSecurityConfigurerAdapter {
//    @Autowired
//    private MyUserDetailsService userDetailsService;
//    @Autowired
//    private DefaultAuthenticationSuccessHandler defaultAuthenticationSuccessHandler;
//    @Autowired
//    private DefaultAuthenticationFailureHandler defaultAuthenticationFailureHandler;
////    @Autowired
////    private ValidateCodeFilter validateCodeFilter;
//    @Autowired
//    private CustomAccessDeniedHandler accessDeniedHandler;
//
//    @Bean
//    public BCryptPasswordEncoder passwordEncoder() {
//        return new BCryptPasswordEncoder();
//    }
//
//    @Override
//    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        auth.userDetailsService(userDetailsService)
//                .passwordEncoder(passwordEncoder());
//    }
//
//    @Override
//    protected void configure(HttpSecurity http) throws Exception {
//        http
//                .formLogin()
//                .loginPage("/login.html")//调转到自定义的登录页
//                .loginProcessingUrl("/login")//登录页向后台发送的登录请求地址
//                .successHandler(defaultAuthenticationSuccessHandler) //登录成功后的处理
//                .failureHandler(defaultAuthenticationFailureHandler)//登录失败后的处理
//                .and()
//                .authorizeRequests()
//                .antMatchers(HttpMethod.GET, "/", "/*.html", "/**/*.html", "/**/*.css", "/**/*.js", "/profile/**").permitAll()
//                // 3. 对于SWAGGER相关信息允许任何用户访问
//                .antMatchers("/swagger-ui.html", "/swagger-resources/**", "/webjars/**", "/*/api-docs", "/druid/**").permitAll()
//                .anyRequest()
//                .authenticated();
//        http.csrf().disable();
//    }
//
//
//    @Override
//    public void configure(WebSecurity web) throws Exception {
//
//        //放行静态资源被拦截
//        web.ignoring().antMatchers("/css/**");
//        web.ignoring().antMatchers("/js/**");
//        web.ignoring().antMatchers("/images/**");
//    }
//}
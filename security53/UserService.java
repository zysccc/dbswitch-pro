package com.gitee.dbswitch.admin.security53;

import com.gitee.dbswitch.admin.dao.UserDao;
import com.gitee.dbswitch.admin.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class UserService  {
	
	@Autowired
	private UserDao userDao;

	public User findUser(Integer id) {
		return userDao.selectByPrimaryKey(id);
	}

}

package com.gitee.dbswitch.product.doris;

public enum DorisPrivilegeEnum {
    ADMIN_PRIV, NODE_PRIV, GRANT_PRIV, SELECT_PRIV, LOAD_PRIV, ALTER_PRIV, CREATE_PRIV, DROP_PRIV, USAGE_PRIV;
}

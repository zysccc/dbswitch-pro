package com.gitee.dbswitch.product.doris;

import org.apache.commons.lang3.StringUtils;

public class DorisUserPrivilegeSqls {
    public static String allGrants() {
        return "SHOW ALL GRANTS;";
    }

    public static String grants() {
        return "SHOW GRANTS;";
    }

    public static String grantsForUser(String user) {
        return "SHOW GRANTS FOR " + user + ";";
    }

    public static String roles() {
        return "SHOW ROLES;";
    }

    public static String catalogs() {
        return "show catalogs;";
    }
    //获取指定用户的表权限
    public static String getUserPrivilege(String user,String catalog,String schema,String tablename){
        StringBuffer buffer = new StringBuffer();
        buffer.append("select replace(split_part(GRANTEE,'@',1),\"'\",'')  as username,replace(split_part(GRANTEE,'@',2),\"'\",'')  as host,TABLE_CATALOG ,TABLE_SCHEMA ,TABLE_NAME ,\n" +
                "PRIVILEGE_TYPE ,IS_GRANTABLE from information_schema.table_privileges where 1=1");
        String and1="and replace(split_part(GRANTEE,'@',1),\"'\",'')='%s'";
        String and2="and TABLE_CATALOG='%s'";
        String and3="and TABLE_SCHEMA='%s'";
        String and4="and TABLE_NAME='%s'";
        //todo 增加参数校验
        buffer.append(String.format(and1,user));
        buffer.append(String.format(and2,catalog));
        buffer.append(String.format(and3,schema));
        buffer.append(String.format(and4,tablename));
        return buffer.toString();
    }
    //获取所有用户
    public static String getUsers(){
        StringBuffer buffer = new StringBuffer();
        buffer.append("select replace(split_part(GRANTEE,'@',1),\"'\",'')  as username,replace(split_part(GRANTEE,'@',2),\"'\",'')  as host,TABLE_CATALOG ,TABLE_SCHEMA ,TABLE_NAME ,\n" +
                "PRIVILEGE_TYPE ,IS_GRANTABLE from information_schema.table_privileges where 1=1");

        return buffer.toString();
    }
    public static String createUser(String user, String password, String role) {
        StringBuffer buffer = new StringBuffer();
        buffer.append("CREATE USER ");
        buffer.append(user);
        if (!StringUtils.isEmpty(password)) {
            buffer.append(" IDENTIFIED BY ");
            buffer.append("'");
            buffer.append(password);
            buffer.append("'");
        }
        if (!StringUtils.isEmpty(role)) {
            buffer.append(" DEFAULT ROLE ");
            buffer.append("'");
            buffer.append(role);
            buffer.append("'");
        }
        buffer.append(";");
        return buffer.toString();
    }

    public static String grantRoleToUser(String role, String user) {
        StringBuffer buffer = new StringBuffer();
        buffer.append("GRANT ");
        buffer.append("'");
        buffer.append(role);
        buffer.append("'");
        buffer.append(" TO ");
        buffer.append(user);
        buffer.append(";");
        return buffer.toString();
    }

    public static String revokeRoleFromUser(String role, String user) {
        StringBuffer buffer = new StringBuffer();
        buffer.append("REVOKE ");
        buffer.append("'");
        buffer.append(role);
        buffer.append("'");
        buffer.append(" FROM ");
        buffer.append(user);
        buffer.append(";");
        return buffer.toString();
    }
    public static String grantPrivilegeToUser(String privileges, String privLevel, String user) {
        StringBuffer buffer = new StringBuffer();
        buffer.append("GRANT ");
        buffer.append(privileges);
        buffer.append(" ON ");
        buffer.append(privLevel);
        buffer.append(" TO ");
        buffer.append(user);
        return buffer.toString();
    }

    public static String grantResourceToUser(String privileges, String resourceName, String user) {
        StringBuffer buffer = new StringBuffer();
        buffer.append("GRANT ");
        buffer.append(privileges);
        buffer.append(" ON RESOURCE ");
        buffer.append(resourceName);
        buffer.append(" TO ");
        buffer.append(user);
        return buffer.toString();
    }

    public static String grantPrivilegeToRole(String privileges, String privLevel, String role) {
        StringBuffer buffer = new StringBuffer();
        buffer.append("GRANT ");
        buffer.append(privileges);
        buffer.append(" ON ");
        buffer.append(privLevel);
        buffer.append(" TO ROLE ");
        buffer.append(role);
        return buffer.toString();
    }
    public static String grantResourceToRole(String privileges, String resourceName, String role) {
        StringBuffer buffer = new StringBuffer();
        buffer.append("GRANT ");
        buffer.append(privileges);
        buffer.append(" ON RESOURCE ");
        buffer.append(resourceName);
        buffer.append(" TO ROLE ");
        buffer.append(role);
        return buffer.toString();
    }
    public static String revokePrivilegeFromUser(String privileges, String privLevel, String user) {
        StringBuffer buffer = new StringBuffer();
        buffer.append("REVOKE ");
        buffer.append(privileges);
        buffer.append(" ON ");
        buffer.append(privLevel);
        buffer.append(" FROM ");
        buffer.append(user);
        return buffer.toString();
    }
    public static String revokeResourceFromUser(String privileges, String resourceName, String user) {
        StringBuffer buffer = new StringBuffer();
        buffer.append("REVOKE ");
        buffer.append(privileges);
        buffer.append(" ON RESOURCE ");
        buffer.append(resourceName);
        buffer.append(" FROM ");
        buffer.append(user);
        return buffer.toString();
    }
    public static String revokePrivilegeFromRole(String privileges, String privLevel, String role) {
        StringBuffer buffer = new StringBuffer();
        buffer.append("REVOKE ");
        buffer.append(privileges);
        buffer.append(" ON ");
        buffer.append(privLevel);
        buffer.append(" FROM ROLE ");
        buffer.append(role);
        return buffer.toString();
    }
    public static String revokeResourceFromRole(String privileges, String resourceName, String role) {
        StringBuffer buffer = new StringBuffer();
        buffer.append("REVOKE ");
        buffer.append(privileges);
        buffer.append(" ON RESOURCE ");
        buffer.append(resourceName);
        buffer.append(" FROM ROLE ");
        buffer.append(role);
        return buffer.toString();
    }
    public static String dropUser(String user) {
        return "DROP USER " + user + ";";
    }
    public static String updatePassword(String user, String password) {
        StringBuffer buffer = new StringBuffer();
        buffer.append("SET PASSWORD FOR ");
        buffer.append(user);
        buffer.append(" = ");
        buffer.append("PASSWORD('");
        buffer.append(password);
        buffer.append("')");
        buffer.append(";");
        return buffer.toString();
    }
    public static String createRole(String role) {
        return "CREATE ROLE " + role + ";";
    }

    public static String dropRole(String role) {
        return "DROP ROLE " + role + ";";
    }

}

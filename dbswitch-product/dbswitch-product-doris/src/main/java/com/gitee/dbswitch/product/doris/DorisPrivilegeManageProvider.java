package com.gitee.dbswitch.product.doris;

import com.gitee.dbswitch.common.privilege.*;
import com.gitee.dbswitch.provider.ProductFactoryProvider;
import com.gitee.dbswitch.provider.privilege.*;
import com.gitee.dbswitch.util.DBSqlUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.doris.nereids.parser.NereidsParser;

import java.sql.Connection;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DorisPrivilegeManageProvider extends AbstractPrivilegeProvider {
    private static final int  DATASIZE = 10000;
    protected DorisPrivilegeManageProvider(ProductFactoryProvider factoryProvider) {
        super(factoryProvider);
    }
    private NereidsParser nereidsParser = new NereidsParser();

    //传入admin权限的conn
    @Override
    public UserInfoArr getUsers(Connection conn, String dbuser) throws Exception {
        DBSqlUtils.ScriptExecuteResult resultSet = null;
        DBSqlUtils.ScriptExecuteResult roleResult = null;
        UserInfoArr result = new UserInfoArr();
        try {
            resultSet = DBSqlUtils.execute(conn, DorisUserPrivilegeSqls.allGrants(), 1, DATASIZE);
            roleResult = DBSqlUtils.execute(conn, DorisUserPrivilegeSqls.roles(), 1, DATASIZE);
            result.setGlobalGrant(true);
        } catch (Exception e) {
            throw e;
        }
        Map<String, String> roleUsers = new HashMap<>();
        if (!Objects.isNull(roleResult))
            for (Map<String, Object> rolerow : roleResult.getResultData()) {
                String roleName = null;
                String userList = null;
                String name = (String) rolerow.get("Name");
                userList = (String) rolerow.get("Users");
                userList = userList.replaceAll("default_cluster:", "");
                if (roleName != null)
                    roleUsers.put(roleName, userList);
            }
        List<UserInfoArr.UserInfo> userInfos = Lists.newArrayList();
        for (Map<String, Object> privilege : resultSet.getResultData()) {
            UserInfoArr.UserInfo userInfo = new UserInfoArr.UserInfo();
            String user = (String) privilege.get("UserIdentity");
            userInfo.setUser(user.replaceAll("default_cluster:", ""));
            List<String> roles = Lists.newArrayList();
            for (String role : roleUsers.keySet()) {
                if (((String) roleUsers.get(role)).contains(userInfo.getUser()))
                    roles.add(role.replaceAll("default_cluster:", ""));
            }
            userInfo.setRoles(roles);
            String[] userHost = user.split("@");
            if (userHost.length != 2)
                throw new Exception("Get user name exception");
            userInfo.setHost(userHost[1].substring(1, userHost[1].length() - 1));
            String userName = userHost[0].substring(1, userHost[0].length() - 1);
            userInfo.setUserName(userName.replaceAll("default_cluster:", ""));
            userInfos.add(userInfo);
        }
        result.setUsers(userInfos);
        return result;
    }

    //传入admin权限的conn
    @Override
    public UserPrivilege getUserOrRolePrivilege(Connection conn,String user, String role) throws Exception {
        UserPrivilege result = new UserPrivilege();
        UserPrivilege.DataPrivilege dataPrivilege = new UserPrivilege.DataPrivilege();
        DBSqlUtils.ScriptExecuteResult userOrRoleResult = null;
        Map<String, Object> row=null;
        if(!StringUtils.isEmpty(user)){
            try {
                userOrRoleResult=DBSqlUtils.execute(conn,DorisUserPrivilegeSqls.grantsForUser(user), 1, DATASIZE);
                row= userOrRoleResult.getResultData().get(0);
            } catch (Exception e) {
                throw e;
            }
        }else{
            try {
                userOrRoleResult=DBSqlUtils.execute(conn,DorisUserPrivilegeSqls.roles(), 1, DATASIZE);
                List<Map<String, Object>> rows = userOrRoleResult.getResultData();
                for(Map<String, Object> tmp1:rows){
                    if(role.equals(tmp1.get("Name"))){
                        row=tmp1;
                        break;
                    }
                }
            } catch (Exception e) {
                throw e;
            }
        }
        String globalStr = (String)row.get("GlobalPrivs");
        List<String> parsePrivs = parsePrivStrs(globalStr);
        if (parsePrivs.size() == 1) {
            List<String> globalPrivs = getPriv(parsePrivs.get(0));
            result.setGlobals(globalPrivs);
        }
        String catalogStr = (String)row.get("CatalogPrivs");
        parsePrivs = parsePrivStrs(catalogStr);
        dataPrivilege.setCatalogs(getCatalogPriv(parsePrivs));
        String dbStr = (String)row.get("DatabasePrivs");
        parsePrivs = parsePrivStrs(dbStr);
        dataPrivilege.setDatabases(getDbPriv(parsePrivs));
        String tableStr = (String)row.get("TablePrivs");
        parsePrivs = parsePrivStrs(tableStr);
        dataPrivilege.setTables(getTablePriv(parsePrivs));
        String resourceStr = (String)row.get("ResourcePrivs");
        parsePrivs = parsePrivStrs(resourceStr);
        result.setResources(getResourcePriv(parsePrivs));
        dataPrivilege.fillIn();
        result.setDatas(dataPrivilege);
        return result;
    }

    @Override
    public Boolean addUserOrRolePrivilege(Connection conn,PrivilegeRequest request, String user, String role) throws Exception {
        request.checkIllegal();
        if ("admin".equals(role)) {
            //log.warn("The admin role can't grant priv.");
            throw new Exception("The admin role can't grant priv.");
        }
        try {
            user = addSingleQuotes(user);
            role = addSingleQuotes(role);
            PrivilegeRequest addPrivilege=request;
            if (Objects.isNull(addPrivilege))
                return true;
            String globalLevel = getGlobalLevel(conn,getDorisVersion(conn));
            if (!StringUtils.isEmpty(user)) {
                if (!StringUtils.isEmpty(addPrivilege.getGlobals())) {
                    String usagePriv = DorisPrivilegeEnum.USAGE_PRIV.name();
                    if (addPrivilege.getGlobals().contains(usagePriv))
                        DBSqlUtils.executeUpdate(conn,DorisUserPrivilegeSqls.grantResourceToUser(usagePriv, "*", user));
                    String newPrivilege = addPrivilege.getGlobals().replace("," + usagePriv, "").replace(usagePriv + ",", "").replace(usagePriv, "");
                    if (!StringUtils.isEmpty(newPrivilege))
                        DBSqlUtils.executeUpdate(conn,DorisUserPrivilegeSqls.grantPrivilegeToUser(newPrivilege, globalLevel, user));
                }
                if (!CollectionUtils.isEmpty(addPrivilege.getCatalogs()))
                    for (CatalogPrivilege catalog : addPrivilege.getCatalogs())
                        DBSqlUtils.executeUpdate(conn, DorisUserPrivilegeSqls.grantPrivilegeToUser(catalog.getPrivilege(),
                                catalog.getKey().toPrivilegeString(), user));
                if (!CollectionUtils.isEmpty(addPrivilege.getDatabases()))
                    for (DbPrivilege db : addPrivilege.getDatabases())
                        DBSqlUtils.executeUpdate(conn,
                                DorisUserPrivilegeSqls.grantPrivilegeToUser(db.getPrivilege(), db.getKey().toPrivilegeString(), user));
                if (!CollectionUtils.isEmpty(addPrivilege.getTables()))
                    for (TablePrivilege table : addPrivilege.getTables())
                        DBSqlUtils.executeUpdate(conn,DorisUserPrivilegeSqls.grantPrivilegeToUser(table.getPrivilege(), table
                                        .getKey().toPrivilegeString(), user));
                if (!CollectionUtils.isEmpty(addPrivilege.getResources()))
                    for (ResourcePrivilege resource : addPrivilege.getResources())
                        DBSqlUtils.executeUpdate(conn, DorisUserPrivilegeSqls.grantResourceToUser(resource.getPrivilege(), resource
                                        .getResource(), user));
            } else if (!StringUtils.isEmpty(role)) {
                if (!StringUtils.isEmpty(addPrivilege.getGlobals())) {
                    String usagePriv = DorisPrivilegeEnum.USAGE_PRIV.name();
                    if (addPrivilege.getGlobals().contains(usagePriv))
                        DBSqlUtils.executeUpdate(conn,
                                DorisUserPrivilegeSqls.grantResourceToRole(usagePriv, "*", role));
                    String newPrivilege = addPrivilege.getGlobals().replace("," + usagePriv, "").replace(usagePriv + ",", "").replace(usagePriv, "");
                    if (!StringUtils.isEmpty(newPrivilege))
                        DBSqlUtils.executeUpdate(conn,
                                DorisUserPrivilegeSqls.grantPrivilegeToRole(newPrivilege, globalLevel, role));
                }
                if (!CollectionUtils.isEmpty(addPrivilege.getCatalogs()))
                    for (CatalogPrivilege catalog : addPrivilege.getCatalogs())
                        DBSqlUtils.executeUpdate(conn,
                                DorisUserPrivilegeSqls.grantPrivilegeToRole(catalog.getPrivilege(), catalog
                                        .getKey().toPrivilegeString(), role));
                if (!CollectionUtils.isEmpty(addPrivilege.getDatabases()))
                    for (DbPrivilege db : addPrivilege.getDatabases())
                        DBSqlUtils.executeUpdate(conn,
                                DorisUserPrivilegeSqls.grantPrivilegeToRole(db.getPrivilege(), db
                                        .getKey().toPrivilegeString(), role));
                if (!CollectionUtils.isEmpty(addPrivilege.getTables()))
                    for (TablePrivilege table : addPrivilege.getTables())
                        DBSqlUtils.executeUpdate(conn,
                                DorisUserPrivilegeSqls.grantPrivilegeToRole(table.getPrivilege(), table
                                        .getKey().toPrivilegeString(), role));
                if (!CollectionUtils.isEmpty(addPrivilege.getResources()))
                    for (ResourcePrivilege resource : addPrivilege.getResources())
                        DBSqlUtils.executeUpdate(conn,
                                DorisUserPrivilegeSqls.grantResourceToRole(resource.getPrivilege(), resource
                                        .getResource(), role));
            } else {
                //log.error("The user and role name all empty.");
                throw new Exception("The user and role name all empty.");
            }
            return Boolean.valueOf(true);
        } catch (Exception e) {
            //log.error("add use or role privilege error", e);
            throw e;
        } finally {

        }

    }

    @Override
    public Boolean revokeUserOrRolePrivilege(Connection conn,PrivilegeRequest revokePrivilege, String user, String role) throws Exception {
        if (Objects.isNull(revokePrivilege))
            return true;
        String globalLevel = getGlobalLevel(conn,getDorisVersion(conn));
        if (!StringUtils.isEmpty(user)) {
            if (!StringUtils.isEmpty(revokePrivilege.getGlobals())) {
                String usagePriv = DorisPrivilegeEnum.USAGE_PRIV.name();
                if (revokePrivilege.getGlobals().contains(usagePriv))
                    DBSqlUtils.executeUpdate(conn,
                            DorisUserPrivilegeSqls.revokeResourceFromUser(usagePriv, "*", user));
                String newPrivilege = revokePrivilege.getGlobals().replace("," + usagePriv, "").replace(usagePriv + ",", "").replace(usagePriv, "");
                if (!StringUtils.isEmpty(newPrivilege))
                    DBSqlUtils.executeUpdate(conn,
                            DorisUserPrivilegeSqls.revokePrivilegeFromUser(newPrivilege, globalLevel, user));
            }
            if (!CollectionUtils.isEmpty(revokePrivilege.getCatalogs()))
                for (CatalogPrivilege catalog : revokePrivilege.getCatalogs())
                    DBSqlUtils.executeUpdate(conn,
                            DorisUserPrivilegeSqls.revokePrivilegeFromUser(catalog.getPrivilege(), catalog
                                    .getKey().toPrivilegeString(), user));
            if (!CollectionUtils.isEmpty(revokePrivilege.getDatabases()))
                for (DbPrivilege db : revokePrivilege.getDatabases())
                    DBSqlUtils.executeUpdate(conn,
                            DorisUserPrivilegeSqls.revokePrivilegeFromUser(db.getPrivilege(), db
                                    .getKey().toPrivilegeString(), user));
            if (!CollectionUtils.isEmpty(revokePrivilege.getTables()))
                for (TablePrivilege table : revokePrivilege.getTables())
                    DBSqlUtils.executeUpdate(conn,
                            DorisUserPrivilegeSqls.revokePrivilegeFromUser(table.getPrivilege(), table
                                    .getKey().toPrivilegeString(), user));
            if (!CollectionUtils.isEmpty(revokePrivilege.getResources()))
                for (ResourcePrivilege resource : revokePrivilege.getResources())
                    DBSqlUtils.executeUpdate(conn,
                            DorisUserPrivilegeSqls.revokeResourceFromUser(resource.getPrivilege(), resource
                                    .getResource(), user));
        } else if (!StringUtils.isEmpty(role)) {
            if (!StringUtils.isEmpty(revokePrivilege.getGlobals())) {
                String usagePriv = DorisPrivilegeEnum.USAGE_PRIV.name();
                if (revokePrivilege.getGlobals().contains(usagePriv))
                    DBSqlUtils.executeUpdate(conn,
                            DorisUserPrivilegeSqls.revokePrivilegeFromRole(usagePriv, "*", role));
                String newPrivilege = revokePrivilege.getGlobals().replace("," + usagePriv, "").replace(usagePriv + ",", "").replace(usagePriv, "");
                if (!StringUtils.isEmpty(newPrivilege))
                    DBSqlUtils.executeUpdate(conn,
                            DorisUserPrivilegeSqls.revokePrivilegeFromRole(newPrivilege, globalLevel, role));
            }
            if (!CollectionUtils.isEmpty(revokePrivilege.getCatalogs()))
                for (CatalogPrivilege catalog : revokePrivilege.getCatalogs())
                    DBSqlUtils.executeUpdate(conn,
                            DorisUserPrivilegeSqls.revokePrivilegeFromRole(catalog.getPrivilege(), catalog
                                    .getKey().toPrivilegeString(), role));
            if (!CollectionUtils.isEmpty(revokePrivilege.getDatabases()))
                for (DbPrivilege db : revokePrivilege.getDatabases())
                    DBSqlUtils.executeUpdate(conn,
                            DorisUserPrivilegeSqls.revokePrivilegeFromRole(db.getPrivilege(), db
                                    .getKey().toPrivilegeString(), role));
            if (!CollectionUtils.isEmpty(revokePrivilege.getTables()))
                for (TablePrivilege table : revokePrivilege.getTables())
                    DBSqlUtils.executeUpdate(conn,
                            DorisUserPrivilegeSqls.revokePrivilegeFromRole(table.getPrivilege(), table
                                    .getKey().toPrivilegeString(), role));
            if (!CollectionUtils.isEmpty(revokePrivilege.getResources()))
                for (ResourcePrivilege resource : revokePrivilege.getResources())
                    DBSqlUtils.executeUpdate(conn,
                            DorisUserPrivilegeSqls.revokeResourceFromRole(resource.getPrivilege(), resource
                                    .getResource(), role));
        } else {
            //log.error("The user and role name all empty.");
            throw new Exception("The user and role name all empty.");
        }
        return null;
    }

    @Override
    public Boolean createUser(Connection conn, CreateUserRequest request) throws Exception {
        if (request.getRoles().contains("operator")) {
            //log.error("The operator role cannot be authorized to other users.");
            throw new Exception("The operator role cannot be authorized to other users.");
        }
        boolean isGreaterThan = isGreaterThanEqualUserRoleSupportVersion(getDorisVersion(conn));
        if (!isGreaterThan && request.getRoles().size() > 1) {
            //log.error("Doris earlier than version 2.0.0 does not support binding multiple roles to one user.");
            throw new Exception("Doris earlier than version 2.0.0 does not support binding multiple roles to one user.");
        }
        String user = addSingleQuotes(request.getName());
        if (!StringUtils.isEmpty(request.getHost()))
            user = user + "@" + addSingleQuotes(request.getHost());
        Set<String> roles = request.getRoles();
        if (roles.contains("admin")) {
            DBSqlUtils.executeUpdate(conn,
                    DorisUserPrivilegeSqls.createUser(user, request
                    .getPassword(), "admin"));
            roles.remove("admin");
        } else {
            DBSqlUtils.executeUpdate(conn,
                    DorisUserPrivilegeSqls.createUser(user, request
                    .getPassword(), null));
        }
        if (CollectionUtils.isNotEmpty(roles))
            for (String role : roles)
                DBSqlUtils.executeUpdate(conn,
                        DorisUserPrivilegeSqls.grantRoleToUser(role, user));
        addUserOrRolePrivilege(conn, request.getAddPrivilege(), user, null);
        revokeUserOrRolePrivilege(conn, request.getRevokePrivilege(), user, null);
        return Boolean.valueOf(true);
    }

    @Override
    public Boolean updateUser(Connection conn, UpdataUserRequest request) throws Exception {
        if (StringUtils.isNotEmpty(request.getPassword())) {
            DBSqlUtils.executeUpdate(conn,
                    DorisUserPrivilegeSqls.updatePassword(addSingleQuotes(request.getName()), request.getPassword()));
        } else {
            if (request.getNewRoles().contains("operator")) {
                //log.error("The operator role cannot be authorized to other users.");
                throw new Exception("The operator role cannot be authorized to other users.");
            }
            try {
                String version = getDorisVersion(conn);
                boolean isGreaterThan = isGreaterThanEqualUserRoleSupportVersion(version);
                if (!isGreaterThan && request.getNewRoles().size() > 1) {
                    //log.error("Doris earlier than version 2.0.0 does not support binding multiple roles to one user.");
                    throw new Exception("Doris earlier than version 2.0.0 does not support binding multiple roles to one user.");
                }
                String user = addSingleQuotes(request.getName());
                Set<String> addRoles = Sets.newHashSet();
                for (String newRole : request.getNewRoles())
                    addRoles.add(newRole);
                Set<String> deleteRoles = request.getOldRoles();
                for (String oldRole : request.getOldRoles())
                    deleteRoles.add(oldRole);
                addRoles.removeAll(request.getOldRoles());
                deleteRoles.removeAll(request.getNewRoles());
                for (String role : deleteRoles) {
                    if (role.equals("operator"))
                        continue;
                    try {
                        DBSqlUtils.executeUpdate(conn,
                                DorisUserPrivilegeSqls.revokeRoleFromUser(role, user));
                    } catch (Exception e) {
                        if (StringUtils.isNotEmpty(e.getMessage()) && e.getMessage().contains("Can not revoke role: admin")) {
                            //log.warn("Doris earlier than 2.0.0 does not support deleting the admin role");
                            continue;
                        }
                        throw e;
                    }
                }
                for (String role : addRoles) {
                    try {
                        DBSqlUtils.executeUpdate(conn,
                                DorisUserPrivilegeSqls.grantRoleToUser(role, user));
                    } catch (Exception e) {
                        if (StringUtils.isNotEmpty(e.getMessage()) && e.getMessage().contains("Can not grant role: admin")) {
                            //log.warn("Doris earlier than 2.0.0 does not support add the admin role");
                            continue;
                        }
                        throw e;
                    }
                }
            } catch (Exception e) {
                //log.error("update use role error", e);
                throw e;
            } finally {
                //this.jdbcProxy.closeConnection(connection, statement);
            }
        }
        return Boolean.valueOf(true);
    }

    public Boolean dropUser(Connection conn,String user) throws Exception {
        DBSqlUtils.executeUpdate(conn,
                DorisUserPrivilegeSqls.dropUser(addSingleQuotes(user)));
        return Boolean.valueOf(true);
    }

    @Override
    public Boolean exeDdl(Connection conn, String user,String sql) throws Exception {
        DBSqlUtils.executeUpdate(conn,
                sql);
        return Boolean.valueOf(true);
    }

    @Override
    public Boolean checkDdl(Connection conn,  String user,String sql) throws Exception {
        nereidsParser.parseSingle(sql);
        return true;
    }

    private List<String> parsePrivStrs(String privStr) {
        if (StringUtils.isEmpty(privStr))
            return Lists.newArrayList();
        String noFalse = privStr.replaceAll("\\(false\\)", "");
        String noNull = noFalse.replaceAll("NULL", "");
        String newPrivStr = noNull.trim();
        if (StringUtils.isEmpty(newPrivStr))
            return Lists.newArrayList();
        String newPrivStrNoCluster = newPrivStr.replaceAll("default_cluster:", "");
        String newPrivStrNoAsterisk = newPrivStrNoCluster.replaceAll("\\.\\*", "");
        String[] privs = newPrivStrNoAsterisk.split(";");
        return Arrays.asList(privs);
    }
    private List<CatalogPrivilege> getCatalogPriv(List<String> catalogPrivs) {
        List<CatalogPrivilege> catalogPrivList = Lists.newArrayList();
        for (String catalogPriv : catalogPrivs) {
            if (StringUtils.isEmpty(catalogPriv))
                continue;
            String[] catalogAndPriv = catalogPriv.split(":");
            if (catalogAndPriv.length < 2) {
                //log.error("The catalog privilege {} format error.", catalogPriv);
                continue;
            }
            catalogPrivList.add(new CatalogPrivilege(new CatalogKey(catalogAndPriv[0].trim().replaceAll("\\.\\*", "")),
                    getPrivString(catalogAndPriv[1])));
        }
        return catalogPrivList;
    }

    private List<DbPrivilege> getDbPriv(List<String> dbPrivs) {
        List<DbPrivilege> dbPrivList = Lists.newArrayList();
        for (String dbPriv : dbPrivs) {
            if (StringUtils.isEmpty(dbPriv))
                continue;
            String[] dbAndPriv = dbPriv.split(":");
            if (dbAndPriv.length < 2) {
                //log.error("The db privilege {} format error.", dbPriv);
                continue;
            }
            String[] catalogAndDb = dbAndPriv[0].split("\\.");
            if (catalogAndDb.length == 2) {
               // log.info("The db privilege {} is for version 1.2 or later.", dbPriv);
                dbPrivList.add(new DbPrivilege(new DbKey(catalogAndDb[0].trim(), catalogAndDb[1].trim()), getPrivString(dbAndPriv[1])));
                continue;
            }
           // log.info("The db privilege {} is for version 1.2 before.", dbPriv);
            dbPrivList.add(new DbPrivilege(new DbKey(null, dbAndPriv[0].trim()), getPrivString(dbAndPriv[1])));
        }
        return dbPrivList;
    }

    private List<TablePrivilege> getTablePriv(List<String> tablePrivs) {
        List<TablePrivilege> tablePrivList = Lists.newArrayList();
        for (String tablePriv : tablePrivs) {
            if (StringUtils.isEmpty(tablePriv))
                continue;
            String[] tableAndPriv = tablePriv.split(":");
            if (tableAndPriv.length != 2) {
                //log.error("The table privilege {} format error.", tablePriv);
                continue;
            }
            String[] catalogAndDbAndTable = tableAndPriv[0].split("\\.");
            if (catalogAndDbAndTable.length == 3) {
               // log.info("The table privilege {} is for version 1.2 or later.", tablePriv);
                tablePrivList.add(new TablePrivilege(new TableKey(catalogAndDbAndTable[0].trim(), catalogAndDbAndTable[1].trim(), catalogAndDbAndTable[2]
                        .trim()), getPrivString(tableAndPriv[1])));
                continue;
            }
            if (catalogAndDbAndTable.length == 2) {
                //log.info("The table privilege {} is for version 1.2 before.", tablePriv);
                tablePrivList.add(new TablePrivilege(new TableKey(null, catalogAndDbAndTable[0].trim(), catalogAndDbAndTable[1]
                        .trim()), getPrivString(tableAndPriv[1])));
                continue;
            }
            //log.error("The table privilege {} format error.", tablePriv);
        }
        return tablePrivList;
    }

    private List<ResourcePrivilege> getResourcePriv(List<String> resourcePrivs) {
        List<ResourcePrivilege> resourcePrivList = Lists.newArrayList();
        for (String resourcePriv : resourcePrivs) {
            if (StringUtils.isEmpty(resourcePriv))
                continue;
            String[] resourceAndPriv = resourcePriv.split(":");
            if (resourceAndPriv.length != 2) {
                //log.error("The resource privilege {} format error.", resourcePriv);
                continue;
            }
            resourcePrivList.add(new ResourcePrivilege(resourceAndPriv[0].trim(), getPrivString(resourceAndPriv[1])));
        }
        return resourcePrivList;
    }

    private String getPrivString(String privs) {
        List<String> privList = getPriv(privs);
        String privListStr = privList.toString();
        return privListStr.substring(1, privListStr.length() - 1).replaceAll(" ", "");
    }

    private List<String> getPriv(String privs) {
        if (StringUtils.isEmpty(privs))
            return Lists.newArrayList();
        String upperPrivs = privs.toUpperCase();
        List<String> result = Lists.newArrayList();
        for (DorisPrivilegeEnum privilegeEnum : DorisPrivilegeEnum.values()) {
            if (upperPrivs.contains(privilegeEnum.name()))
                result.add(privilegeEnum.name());
        }
        return result;
    }
    private String addSingleQuotes(String str) {
        if (StringUtils.isEmpty(str))
            return null;
        if (str.contains("'"))
            return str;
        return "'" + str + "'";
    }
    public  String getGlobalLevel(Connection conn,String version){
        boolean isGreaterThan = isGreaterThanEqualCatalogSupportVersion(getDorisVersion(conn));
        if (isGreaterThan)
            return "*.*.*";
        return "*.*";
    }
    public String getDorisVersion(Connection conn) {
        try {
            DBSqlUtils.ScriptExecuteResult executeResult = DBSqlUtils.execute(conn, "show variables like \"%version_comment%\";", 1, DATASIZE);
            Map<String, Object> versionObject = executeResult.getResultData().get(0);
            return  (String) versionObject.get("Value");
        } catch (Exception e) {
            //log.error("Get cluster {} version exception", connectionKey.getAddress(), e);
            return "";
        }
    }
    public static boolean isGreaterThanEqualCatalogSupportVersion(String versionStr) {
        if (StringUtils.isEmpty(versionStr)) {
            //log.warn("Cluster {} version string is empty.", clusterId);
            return false;
        }
        String versionInfo = extractVersion(versionStr);
        boolean isGreaterThan = isGreaterThanEqual(versionInfo, "1.2.0");
        return isGreaterThan;
    }
    public boolean isGreaterThanEqualUserRoleSupportVersion(String versionStr) {
        if (StringUtils.isEmpty(versionStr)) {
            //log.warn("Cluster {} version string is empty.", clusterId);
            return false;
        }
        String versionInfo = extractVersion(versionStr);
        boolean isGreaterThan = isGreaterThanEqual(versionInfo, "2.0.0");
        return isGreaterThan;
    }
    private static String extractVersion(String versionString) {
        boolean isSelectDBCore = versionString.startsWith("selectdb-enterprise-core-");
        Pattern pattern = Pattern.compile("\\d+\\.\\d+(\\.\\d+)*");
        Matcher matcher = pattern.matcher(versionString);
        if (matcher.find()) {
            String version = matcher.group();
            if (isSelectDBCore) {
                String[] versionDigits = version.split("\\.");
                if (versionDigits.length == 4 && Objects.equals("2", versionDigits[0]))
                    version = versionDigits[1] + "." + versionDigits[2] + "." + versionDigits[3];
            }
            return version;
        }
       // log.warn("The cluster {} version string:{} error.", clusterId, versionString);
        return "";
    }
    private static boolean isGreaterThanEqual(String version1, String version2) {
        if (StringUtils.isEmpty(version1))
            return false;
        if (StringUtils.isEmpty(version2))
            return true;
        String[] parts1 = version1.split("\\.");
        String[] parts2 = version2.split("\\.");
        for (int i = 0; i < Math.min(parts1.length, parts2.length); i++) {
            int part1 = Integer.parseInt(parts1[i]);
            int part2 = Integer.parseInt(parts2[i]);
            if (part1 > part2)
                return true;
            if (part1 < part2)
                return false;
        }
        return (parts1.length >= parts2.length);
    }

}

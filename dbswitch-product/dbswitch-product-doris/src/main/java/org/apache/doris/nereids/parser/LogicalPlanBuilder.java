// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package org.apache.doris.nereids.parser;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import org.antlr.v4.runtime.RuleContext;
import org.apache.doris.catalog.KeysType;
import org.apache.doris.common.FeConstants;
import org.apache.doris.nereids.DorisParser;
import org.apache.doris.nereids.DorisParserBaseVisitor;
import org.apache.doris.nereids.exceptions.AnalysisException;
import org.apache.doris.nereids.trees.plans.commands.info.DistributionDescriptor;
import org.apache.doris.nereids.trees.plans.logical.LogicalPlan;

import java.util.List;
import java.util.Map;

/**
 * Build a logical plan tree with unbounded nodes.
 */
public class LogicalPlanBuilder extends DorisParserBaseVisitor<Object> {
    /**
     * Create a Sequence of Strings for a parenthesis enclosed alias list.
     */
    @Override
    public List<String> visitIdentifierList(DorisParser.IdentifierListContext ctx) {
        return visitIdentifierSeq(ctx.identifierSeq());
    }
    /**
     * Create a Sequence of Strings for an identifier list.
     */
    @Override
    public List<String> visitIdentifierSeq(DorisParser.IdentifierSeqContext ctx) {
        return ctx.ident.stream()
                .map(RuleContext::getText)
                .collect(ImmutableList.toImmutableList());
    }

    @Override
    public List<String> visitMultipartIdentifier(DorisParser.MultipartIdentifierContext ctx) {
        return ctx.parts.stream()
                .map(RuleContext::getText)
                .collect(ImmutableList.toImmutableList());
    }
    @Override
    public Map<String, String> visitPropertyClause(DorisParser.PropertyClauseContext ctx) {
        return ctx == null ? ImmutableMap.of() : visitPropertyItemList(ctx.fileProperties);
    }
    @Override
    public Map<String, String> visitPropertyItemList(DorisParser.PropertyItemListContext ctx) {
        if (ctx == null || ctx.properties == null) {
            return ImmutableMap.of();
        }
        ImmutableMap.Builder<String, String> propertiesMap = ImmutableMap.builder();
        for (DorisParser.PropertyItemContext argument : ctx.properties) {
            if(argument.key==null||argument.value==null){
                throw new AnalysisException("Property should not be NULL");
            }
            String key = parsePropertyKey(argument.key);
            String value = parsePropertyValue(argument.value);
            propertiesMap.put(key, value);
        }
        return propertiesMap.build();
    }


    private String parsePropertyValue(DorisParser.PropertyValueContext item) {
        if (item.constant() != null) {
            //return parseConstant(item.constant());
        }
        return item.getText();
    }

    @Override
    public LogicalPlan visitCreateTable(DorisParser.CreateTableContext ctx) {
        String ctlName = null;
        String dbName = null;
        String tableName = null;
        List<String> nameParts = visitMultipartIdentifier(ctx.name);
        // TODO: support catalog
        if (nameParts.size() == 1) {
            tableName = nameParts.get(0);
        } else if (nameParts.size() == 2) {
            dbName = nameParts.get(0);
            tableName = nameParts.get(1);
        } else if (nameParts.size() == 3) {
            ctlName = nameParts.get(0);
            dbName = nameParts.get(1);
            tableName = nameParts.get(2);
        } else {
            throw new AnalysisException("nameParts in create table should be [ctl.][db.]tbl");
        }
        KeysType keysType = null;
        if (ctx.DUPLICATE() != null) {
            keysType = KeysType.DUP_KEYS;
        } else if (ctx.AGGREGATE() != null) {
            keysType = KeysType.AGG_KEYS;
        } else if (ctx.UNIQUE() != null) {
            keysType = KeysType.UNIQUE_KEYS;
        }
        String engineName = ctx.engine != null ? ctx.engine.getText().toLowerCase() : "olap";
        int bucketNum = FeConstants.default_bucket_num;
        if (ctx.INTEGER_VALUE() != null) {
            bucketNum = Integer.parseInt(ctx.INTEGER_VALUE().getText());
        }else{
            throw new AnalysisException("bucketNum should be set to an integer ");
        }
        DistributionDescriptor desc = null;
        if (ctx.HASH() != null) {
            if(ctx.autoBucket != null){
                throw new AnalysisException("bucketNum should be set to an integer ");
            }
            desc = new DistributionDescriptor(true, ctx.autoBucket != null, bucketNum,
                    visitIdentifierList(ctx.hashKeys));
        } else if (ctx.RANDOM() != null) {
            throw new AnalysisException("DistributionBy should be HASH ");
           // desc = new DistributionDescriptor(false, ctx.autoBucket != null, bucketNum, null);
        }
        Map<String, String> properties = ctx.properties != null
                // NOTICE: we should not generate immutable map here, because it will be modified when analyzing.
                ? Maps.newHashMap(visitPropertyClause(ctx.properties))
                : Maps.newHashMap();
        Map<String, String> extProperties = ctx.extProperties != null
                // NOTICE: we should not generate immutable map here, because it will be modified when analyzing.
                ? Maps.newHashMap(visitPropertyClause(ctx.extProperties))
                : Maps.newHashMap();
        String partitionType = null;
        if (ctx.PARTITION() != null) {
            partitionType = ctx.RANGE() != null ? "RANGE" : "LIST";
        }
        boolean isAutoPartition = ctx.autoPartition != null;
        //ImmutableList.Builder<Expression> autoPartitionExpr = new ImmutableList.Builder<>();
        if (isAutoPartition) {
            throw new AnalysisException(
                    "PROHIBITED AUTO PARTITION");
        }
        if (ctx.columnDefs() != null) {
            if (ctx.AS() != null) {
                throw new AnalysisException("Should not define the entire column in CTAS");
            }
            return null;
        } else if (ctx.AS() != null) {
            return null;
        } else {
            throw new AnalysisException("Should contain at least one column in a table");
        }
    }
    private String parsePropertyKey(DorisParser.PropertyKeyContext item) {
        if (item.constant() != null) {
            //return parseConstant(item.constant());
        }
        return item.getText();
    }
//    private String parseConstant(DorisParser.ConstantContext context) {
//        Object constant = visit(context);
//        if (constant instanceof Literal && ((Literal) constant).isStringLikeLiteral()) {
//            return ((Literal) constant).getStringValue();
//        }
//        return context.getText();
//    }
}

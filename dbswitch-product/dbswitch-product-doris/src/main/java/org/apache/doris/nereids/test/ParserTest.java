package org.apache.doris.nereids.test;

import org.apache.doris.nereids.parser.NereidsParser;
import org.apache.doris.nereids.trees.plans.logical.LogicalPlan;

public class ParserTest {
    public static void main(String[] args) {
        NereidsParser nereidsParser = new NereidsParser();
        String sql = "SELECT * FROM test;";
        String sql1="CREATE TABLE `test01` (   `user_id` int NULL,   `name` text NULL,   `age` int NULL ) ENGINE=OLAP DUPLICATE KEY(`user_id`) DISTRIBUTED BY HASH(`user_id`) BUCKETS 10 PROPERTIES ( \"light_schema_change\" = \"true\");";
       String sql2="CREATE table  `test01` (   `user_id` int NULL,   `name` text NULL,   `age` int NULL ) ENGINE=OLAP DUPLICATE KEY(`user_id`) DISTRIBUTED BY HASH(`user_id`) BUCKETS 10 PROPERTIES (\"light_schema_change\" = \"true\");";
        Exception exceptionOccurred = null;
        try {
            //Object parse = nereidsParser.parse(sql1, DorisParser::singleStatement);
            //Object parse = nereidsParser.parse(sql1,DorisParser::singleStatement );
            createTables(true,sql2);
            System.out.println("parse");
        } catch (Exception e) {
            exceptionOccurred = e;
            e.printStackTrace();
        }
        //Assertions.assertNull(exceptionOccurred);
    }
    public static void createTables(boolean enableNereids, String... sqls) throws Exception {
        if (enableNereids) {
            for (String sql : sqls) {
                NereidsParser nereidsParser = new NereidsParser();
                LogicalPlan parsed = nereidsParser.parseSingle(sql);
               // StmtExecutor stmtExecutor = new StmtExecutor(connectContext, sql);
//                if (parsed instanceof CreateTableCommand) {
//                    //((CreateTableCommand) parsed).run(connectContext, stmtExecutor);
//                }
            }
        } else {
            for (String sql : sqls) {
                //CreateTableStmt stmt = (CreateTableStmt) parseAndAnalyzeStmt(sql);
               // Env.getCurrentEnv().createTable(stmt);
            }
        }
        //updateReplicaPathHash();
    }
}

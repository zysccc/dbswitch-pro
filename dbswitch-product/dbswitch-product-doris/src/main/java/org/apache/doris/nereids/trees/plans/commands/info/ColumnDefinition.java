// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package org.apache.doris.nereids.trees.plans.commands.info;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import org.apache.doris.catalog.KeysType;
import org.apache.doris.catalog.ScalarType;
import org.apache.doris.catalog.Type;
import org.apache.doris.nereids.exceptions.AnalysisException;
import org.apache.doris.nereids.types.*;

import java.util.*;

/**
 * column definition
 * TODO: complex types will not work, we will support them later.
 */
public class ColumnDefinition {
    private final String name;
    private DataType type;
    private boolean isKey;
    private AggregateType aggType;
    private boolean isNullable;
    private Optional<DefaultValue> defaultValue;
    private Optional<DefaultValue> onUpdateDefaultValue = Optional.empty();
    private final String comment;
    private final boolean isVisible;
    private boolean aggTypeImplicit = false;
    private boolean isAutoInc = false;
    private int clusterKeyId = -1;

    public ColumnDefinition(String name, DataType type, boolean isKey, AggregateType aggType, boolean isNullable,
                            Optional<DefaultValue> defaultValue, String comment) {
        this(name, type, isKey, aggType, isNullable, defaultValue, comment, true);
    }

    public ColumnDefinition(String name, DataType type, boolean isKey, AggregateType aggType,
                            boolean isNullable, boolean isAutoInc, Optional<DefaultValue> defaultValue,
                            Optional<DefaultValue> onUpdateDefaultValue, String comment) {
        this(name, type, isKey, aggType, isNullable, isAutoInc, defaultValue, onUpdateDefaultValue,
                comment, true);
    }

    /**
     * constructor
     */
    public ColumnDefinition(String name, DataType type, boolean isKey, AggregateType aggType, boolean isNullable,
                            Optional<DefaultValue> defaultValue, String comment, boolean isVisible) {
        this.name = name;
        this.type = type;
        this.isKey = isKey;
        this.aggType = aggType;
        this.isNullable = isNullable;
        this.defaultValue = defaultValue;
        this.comment = comment;
        this.isVisible = isVisible;
    }

    /**
     * constructor
     */
    private ColumnDefinition(String name, DataType type, boolean isKey, AggregateType aggType,
                             boolean isNullable, boolean isAutoInc, Optional<DefaultValue> defaultValue,
                             Optional<DefaultValue> onUpdateDefaultValue, String comment, boolean isVisible) {
        this.name = name;
        this.type = type;
        this.isKey = isKey;
        this.aggType = aggType;
        this.isNullable = isNullable;
        this.isAutoInc = isAutoInc;
        this.defaultValue = defaultValue;
        this.onUpdateDefaultValue = onUpdateDefaultValue;
        this.comment = comment;
        this.isVisible = isVisible;
    }

    public ColumnDefinition(String name, DataType type, boolean isNullable) {
        this(name, type, false, null, isNullable, Optional.empty(), "");
    }

    public ColumnDefinition(String name, DataType type, boolean isNullable, String comment) {
        this(name, type, false, null, isNullable, Optional.empty(), comment);
    }

    public String getName() {
        return name;
    }

    public DataType getType() {
        return type;
    }

    public AggregateType getAggType() {
        return aggType;
    }

    public void setAggType(AggregateType aggType) {
        this.aggType = aggType;
    }

    public boolean isNullable() {
        return isNullable;
    }

    public boolean isKey() {
        return isKey;
    }

    public void setIsKey(boolean isKey) {
        this.isKey = isKey;
    }

    public void setClusterKeyId(int clusterKeyId) {
        this.clusterKeyId = clusterKeyId;
    }

    public boolean isAutoInc() {
        return isAutoInc;
    }

    private DataType updateCharacterTypeLength(DataType dataType) {
        if (dataType instanceof ArrayType) {
            return ArrayType.of(updateCharacterTypeLength(((ArrayType) dataType).getItemType()));
        } else if (dataType instanceof MapType) {
            DataType keyType = updateCharacterTypeLength(((MapType) dataType).getKeyType());
            DataType valueType = updateCharacterTypeLength(((MapType) dataType).getValueType());
            return MapType.of(keyType, valueType);
        } else if (dataType instanceof StructType) {
            List<StructField> structFields = ((StructType) dataType).getFields().stream()
                    .map(sf -> sf.withDataType(updateCharacterTypeLength(sf.getDataType())))
                    .collect(ImmutableList.toImmutableList());
            return new StructType(structFields);
        } else {
            if (dataType.isStringLikeType()) {
                if (dataType instanceof CharType && ((CharType) dataType).getLen() == -1) {
                    return new CharType(1);
                } else if (dataType instanceof VarcharType && ((VarcharType) dataType).getLen() == -1) {
                    return new VarcharType(VarcharType.MAX_VARCHAR_LENGTH);
                }
            }
            return dataType;
        }
    }

    /**
     * validate column definition and analyze
     */
    public void validate(boolean isOlap, Set<String> keysSet, boolean isEnableMergeOnWrite, KeysType keysType) {
        validateDataType(type.toCatalogDataType());
        type = updateCharacterTypeLength(type);
        if (type.isArrayType()) {
            int depth = 0;
            DataType curType = type;
            while (curType.isArrayType()) {
                curType = ((ArrayType) curType).getItemType();
                depth++;
            }
            if (depth > 9) {
                throw new AnalysisException("Type exceeds the maximum nesting depth of 9");
            }
        }


        // check keys type
        if (keysSet.contains(name)) {
            isKey = true;
            if (aggType != null) {
                throw new AnalysisException(
                        String.format("Key column %s can not set aggregation type", name));
            }
            if (isOlap) {
                if (type.isFloatLikeType()) {
                    throw new AnalysisException(
                            "Float or double can not used as a key, use decimal instead.");
                } else if (type.isStringType()) {
                    throw new AnalysisException(
                            "String Type should not be used in key column[" + name + "]");
                } else if (type.isArrayType()) {
                    throw new AnalysisException("Array can only be used in the non-key column of"
                            + " the duplicate table at present.");
                }
            }
            if (type.isHllType() || type.isQuantileStateType()) {
                throw new AnalysisException("Key column can not set complex type:" + name);
            } else if (type.isJsonType()) {
                throw new AnalysisException(
                        "JsonType type should not be used in key column[" + getName() + "].");
            } else if (type.isMapType()) {
                throw new AnalysisException("Map can only be used in the non-key column of"
                        + " the duplicate table at present.");
            } else if (type.isStructType()) {
                throw new AnalysisException("Struct can only be used in the non-key column of"
                        + " the duplicate table at present.");
            }
        } else if (aggType == null && isOlap) {
            Preconditions.checkState(keysType != null, "keysType is null");
            if (keysType.equals(KeysType.DUP_KEYS)) {
                aggType = AggregateType.NONE;
            } else if (keysType.equals(KeysType.UNIQUE_KEYS) && isEnableMergeOnWrite) {
                aggType = AggregateType.NONE;
            } else if (!keysType.equals(KeysType.AGG_KEYS)) {
                aggType = AggregateType.REPLACE;
            } else {
                throw new AnalysisException("should set aggregation type to non-key column when in aggregate key");
            }
        }

        if (isOlap) {
            if (!isKey && keysType.equals(KeysType.UNIQUE_KEYS)) {
                aggTypeImplicit = true;
            }

            // If aggregate type is REPLACE_IF_NOT_NULL, we set it nullable.
            // If default value is not set, we set it NULL
            if (aggType == AggregateType.REPLACE_IF_NOT_NULL) {
                isNullable = true;
                if (!defaultValue.isPresent()) {
                }
            }
        }

        // check default value
        if (type.isHllType()) {
            if (defaultValue.isPresent()) {
                throw new AnalysisException("Hll type column can not set default value");
            }
        }  else if (type.isMapType()) {

        } else if (type.isStructType()) {

        }

        }

    /**
     * check if is nested complex type.
     */
    private boolean isNestedComplexType(DataType dataType) {
        if (!dataType.isComplexType()) {
            return false;
        }
        if (dataType instanceof ArrayType) {
            if (((ArrayType) dataType).getItemType() instanceof ArrayType) {
                return isNestedComplexType(((ArrayType) dataType).getItemType());
            } else {
                return ((ArrayType) dataType).getItemType().isComplexType();
            }
        }
        if (dataType instanceof MapType) {
            return ((MapType) dataType).getKeyType().isComplexType()
                    || ((MapType) dataType).getValueType().isComplexType();
        }
        if (dataType instanceof StructType) {
            return ((StructType) dataType).getFields().stream().anyMatch(f -> f.getDataType().isComplexType());
        }
        return false;
    }

    // from TypeDef.java analyze()
    private void validateDataType(Type catalogType) {
        if (catalogType.exceedsMaxNestingDepth()) {
            throw new AnalysisException(
                    String.format("Type exceeds the maximum nesting depth of %s:\n%s",
                            Type.MAX_NESTING_DEPTH, catalogType.toSql()));
        }
        if (!catalogType.isSupported()) {
            throw new AnalysisException("Unsupported data type: " + catalogType.toSql());
        }

        if (catalogType.isScalarType()) {
            validateScalarType((ScalarType) catalogType);
        } else if (catalogType.isComplexType()) {
            // now we not support array / map / struct nesting complex type
            if (catalogType.isArrayType()) {
                Type itemType = ((org.apache.doris.catalog.ArrayType) catalogType).getItemType();
                if (itemType instanceof ScalarType) {
                    validateNestedType(catalogType, (ScalarType) itemType);
                }
            }
            if (catalogType.isMapType()) {
                org.apache.doris.catalog.MapType mt =
                        (org.apache.doris.catalog.MapType) catalogType;
//                if (Config.disable_nested_complex_type && (!(mt.getKeyType() instanceof ScalarType)
//                        || !(mt.getValueType() instanceof ScalarType))) {
//                    throw new AnalysisException("Unsupported data type: MAP<"
//                            + mt.getKeyType().toSql() + "," + mt.getValueType().toSql() + ">");
//                }
                if (mt.getKeyType() instanceof ScalarType) {
                    validateNestedType(catalogType, (ScalarType) mt.getKeyType());
                }
                if (mt.getValueType() instanceof ScalarType) {
                    validateNestedType(catalogType, (ScalarType) mt.getValueType());
                }
            }
//            if (catalogType.isStructType()) {
//                ArrayList<org.apache.doris.catalog.StructField> fields =
//                        ((org.apache.doris.catalog.StructType) catalogType).getFields();
//                Set<String> fieldNames = new HashSet<>();
//                for (org.apache.doris.catalog.StructField field : fields) {
//                    Type fieldType = field.getType();
//                    if (fieldType instanceof ScalarType) {
//                        validateNestedType(catalogType, (ScalarType) fieldType);
//                        if (!fieldNames.add(field.getName())) {
//                            throw new AnalysisException("Duplicate field name " + field.getName()
//                                    + " in struct " + catalogType.toSql());
//                        }
//                    } else if (Config.disable_nested_complex_type) {
//                        throw new AnalysisException(
//                                "Unsupported field type: " + fieldType.toSql() + " for STRUCT");
//                    }
//                }
//            }
        }
    }

    private void validateScalarType(ScalarType scalarType) {

    }

    private void validateNestedType(Type parent, Type child) throws AnalysisException {
        if (child.isNull()) {
            throw new AnalysisException("Unsupported data type: " + child.toSql());
        }
        // check whether the sub-type is supported
        if (!parent.supportSubType(child)) {
            throw new AnalysisException(
                    parent.getPrimitiveType() + " unsupported sub-type: " + child.toSql());
        }
        validateDataType(child);
    }




}

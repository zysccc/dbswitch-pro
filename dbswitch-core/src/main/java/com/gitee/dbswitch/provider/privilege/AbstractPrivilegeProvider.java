package com.gitee.dbswitch.provider.privilege;

import com.gitee.dbswitch.provider.AbstractCommonProvider;
import com.gitee.dbswitch.provider.ProductFactoryProvider;

import java.sql.Connection;
import java.util.List;

/**
 * 权限管理抽象基类
 */
public abstract class AbstractPrivilegeProvider extends AbstractCommonProvider implements PrivilegeProvider{

    protected AbstractPrivilegeProvider(ProductFactoryProvider factoryProvider) {
        super(factoryProvider);
    }
}

package com.gitee.dbswitch.provider.privilege;

import com.gitee.dbswitch.common.privilege.*;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * 权限管理
 */
public interface PrivilegeProvider {
    UserInfoArr getUsers(Connection conn, String dbuser) throws  Exception;
    UserPrivilege getUserOrRolePrivilege(Connection conn, String user, String role) throws Exception;
    Boolean addUserOrRolePrivilege(Connection conn,PrivilegeRequest request, String user, String role) throws Exception;
    Boolean revokeUserOrRolePrivilege(Connection conn,PrivilegeRequest request, String user, String role) throws Exception;
    Boolean createUser(Connection conn, CreateUserRequest request) throws Exception;
    Boolean updateUser(Connection conn, UpdataUserRequest request) throws Exception;
    Boolean dropUser(Connection conn,String user) throws Exception;
    Boolean exeDdl(Connection conn,String user,String sql) throws Exception;
    Boolean checkDdl(Connection conn,String user,String sql) throws Exception;


}

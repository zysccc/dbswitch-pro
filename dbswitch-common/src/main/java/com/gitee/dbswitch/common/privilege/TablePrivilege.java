package com.gitee.dbswitch.common.privilege;

import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.Objects;

public class TablePrivilege implements Serializable {
    private TableKey key;

    private String privilege;

    public void setKey(TableKey key) {
        this.key = key;
    }

    public void setPrivilege(String privilege) {
        this.privilege = privilege;
    }


    protected boolean canEqual(Object other) {
        return other instanceof TablePrivilege;
    }


    public String toString() {
        return "TablePrivilege(key=" + getKey() + ", privilege=" + getPrivilege() + ")";
    }

    public TablePrivilege(TableKey key, String privilege) {
        this.key = key;
        this.privilege = privilege;
    }

    public TablePrivilege() {}

    public TableKey getKey() {
        return this.key;
    }

    public String getPrivilege() {
        return this.privilege;
    }

    public void checkIllegal() throws Exception {
        if (StringUtils.isEmpty(this.privilege) || Objects.isNull(this.key))
            throw new Exception("the table key or privilege is empty.");
        this.key.checkIllegal();
    }
}

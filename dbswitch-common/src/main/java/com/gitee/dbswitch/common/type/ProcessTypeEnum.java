package com.gitee.dbswitch.common.type;

public enum ProcessTypeEnum {
    CRETETABLE("CREATETABLE"),
    GRANTTABLE("GRANTTABLE"),
    ;

    ProcessTypeEnum(String typename) {
        this.typename = typename;
    }

    private String typename;

    public String getTypename() {
        return typename;
    }

}

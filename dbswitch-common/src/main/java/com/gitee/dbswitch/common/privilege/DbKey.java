package com.gitee.dbswitch.common.privilege;

import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

public class DbKey implements Serializable {
    private String catalog;

    private String db;

    public void setCatalog(String catalog) {
        this.catalog = catalog;
    }

    public void setDb(String db) {
        this.db = db;
    }

    public String toString() {
        return "DbKey(catalog=" + getCatalog() + ", db=" + getDb() + ")";
    }

    public DbKey(String catalog, String db) {
        this.catalog = catalog;
        this.db = db;
    }

    public DbKey() {}

    public String getCatalog() {
        return this.catalog;
    }

    public String getDb() {
        return this.db;
    }

    public void checkIllegal() throws Exception {
        if (StringUtils.isEmpty(this.db))
            throw new Exception("the database name is empty.");
    }

    public CatalogKey catalogKeyGet() {
        return new CatalogKey(this.catalog);
    }

    public String toPrivilegeString() {
        StringBuffer buffer = new StringBuffer();
        if (this.catalog != null) {
            buffer.append("`");
            buffer.append(this.catalog);
            buffer.append("`");
            buffer.append(".");
        }
        buffer.append("`");
        buffer.append(this.db);
        buffer.append("`");
        buffer.append(".*");
        return buffer.toString();
    }

    public int hashCode() {
        int hashCode = 0;
        if (this.db != null)
            hashCode += this.db.hashCode();
        if (this.catalog != null)
            hashCode += this.catalog.hashCode();
        return hashCode;
    }

    public boolean equals(Object other) {
        if (this == other)
            return true;
        if (other == null)
            return false;
        if (other instanceof DbKey) {
            DbKey otherKey = (DbKey)other;
            if (this.db != null) {
                if (!this.db.equals(otherKey.getDb()))
                    return false;
            } else if (null != otherKey.getDb()) {
                return false;
            }
            if (this.catalog != null) {
                if (!this.catalog.equals(otherKey.getCatalog()))
                    return false;
            } else if (null != otherKey.getCatalog()) {
                return false;
            }
        } else {
            return false;
        }
        return true;
    }
}

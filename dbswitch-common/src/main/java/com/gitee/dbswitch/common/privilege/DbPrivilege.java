package com.gitee.dbswitch.common.privilege;

import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.Objects;

public class DbPrivilege implements Serializable {
    private DbKey key;

    private String privilege;

    public void setKey(DbKey key) {
        this.key = key;
    }

    public void setPrivilege(String privilege) {
        this.privilege = privilege;
    }


    protected boolean canEqual(Object other) {
        return other instanceof DbPrivilege;
    }


    public String toString() {
        return "DbPrivilege(key=" + getKey() + ", privilege=" + getPrivilege() + ")";
    }

    public DbPrivilege(DbKey key, String privilege) {
        this.key = key;
        this.privilege = privilege;
    }

    public DbPrivilege() {}

    public DbKey getKey() {
        return this.key;
    }

    public String getPrivilege() {
        return this.privilege;
    }

    public void checkIllegal() throws Exception {
        if (StringUtils.isEmpty(this.privilege) || Objects.isNull(this.key))
            throw new Exception("the database key or privilege is empty.");
        this.key.checkIllegal();
    }
}

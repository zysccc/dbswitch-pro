package com.gitee.dbswitch.common.privilege;

import com.google.common.collect.Lists;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class UserPrivilege implements Serializable {
    private List<String> globals;

    private DataPrivilege datas;

    private List<ResourcePrivilege> resources;

    public void setGlobals(List<String> globals) {
        this.globals = globals;
    }

    public void setDatas(DataPrivilege datas) {
        this.datas = datas;
    }

    public void setResources(List<ResourcePrivilege> resources) {
        this.resources = resources;
    }

    public String toString() {
        return "UserPrivilegeResponse(globals=" + getGlobals() + ", datas=" + getDatas() + ", resources=" + getResources() + ")";
    }

    public List<String> getGlobals() {
        return this.globals;
    }

    public DataPrivilege getDatas() {
        return this.datas;
    }

    public List<ResourcePrivilege> getResources() {
        return this.resources;
    }

    public static class DataPrivilege implements Serializable {
        public void setCatalogs(List<CatalogPrivilege> catalogs) {
            this.catalogs = catalogs;
        }

        public void setDatabases(List<DbPrivilege> databases) {
            this.databases = databases;
        }

        public void setTables(List<TablePrivilege> tables) {
            this.tables = tables;
        }

        public String toString() {
            return "UserPrivilegeResponse.DataPrivilege(catalogs=" + getCatalogs() + ", databases=" + getDatabases() + ", tables=" + getTables() + ")";
        }

        private List<CatalogPrivilege> catalogs = Lists.newArrayList();

        public List<CatalogPrivilege> getCatalogs() {
            return this.catalogs;
        }

        private List<DbPrivilege> databases = Lists.newArrayList();

        public List<DbPrivilege> getDatabases() {
            return this.databases;
        }

        private List<TablePrivilege> tables = Lists.newArrayList();

        public List<TablePrivilege> getTables() {
            return this.tables;
        }

        public void fillIn() {
            Set<CatalogKey> catalogKeys = (Set<CatalogKey>)this.catalogs.stream().map(catalogPrivilege -> catalogPrivilege.getKey()).collect(Collectors.toSet());
            Set<DbKey> existDbKeys = (Set<DbKey>)this.databases.stream().map(dbPrivilege -> dbPrivilege.getKey()).collect(Collectors.toSet());
            for (TablePrivilege tablePrivilege : this.tables) {
                DbKey dbKey = tablePrivilege.getKey().dbKeyGet();
                if (!existDbKeys.contains(dbKey)) {
                    DbPrivilege dbPrivilege = new DbPrivilege(dbKey, "");
                    this.databases.add(dbPrivilege);
                    existDbKeys.add(dbKey);
                }
                CatalogKey catalogKey = tablePrivilege.getKey().catalogKeyGet();
                if (Objects.isNull(catalogKey.getCatalog()))
                    continue;
                if (!catalogKeys.contains(catalogKey)) {
                    CatalogPrivilege catalogPrivilege = new CatalogPrivilege(catalogKey, "");
                    this.catalogs.add(catalogPrivilege);
                    catalogKeys.add(catalogKey);
                }
            }
            for (DbPrivilege dbPrivilege : this.databases) {
                CatalogKey catalogKey = dbPrivilege.getKey().catalogKeyGet();
                if (Objects.isNull(catalogKey.getCatalog()))
                    continue;
                if (!catalogKeys.contains(catalogKey)) {
                    CatalogPrivilege catalogPrivilege = new CatalogPrivilege(catalogKey, "");
                    this.catalogs.add(catalogPrivilege);
                    catalogKeys.add(catalogKey);
                }
            }
        }
    }
}


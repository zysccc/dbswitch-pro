package com.gitee.dbswitch.common.privilege;

import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

public class TableKey implements Serializable {
    private String catalog;

    private String db;

    private String table;

    public void setCatalog(String catalog) {
        this.catalog = catalog;
    }

    public void setDb(String db) {
        this.db = db;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public String toString() {
        return "TableKey(catalog=" + getCatalog() + ", db=" + getDb() + ", table=" + getTable() + ")";
    }

    public TableKey(String catalog, String db, String table) {
        this.catalog = catalog;
        this.db = db;
        this.table = table;
    }

    public TableKey() {}

    public String getCatalog() {
        return this.catalog;
    }

    public String getDb() {
        return this.db;
    }

    public String getTable() {
        return this.table;
    }

    public void checkIllegal() throws Exception {
        if (StringUtils.isEmpty(this.db) || StringUtils.isEmpty(this.table))
            throw new Exception("the database or table name is empty.");
    }

    public DbKey dbKeyGet() {
        return new DbKey(this.catalog, this.db);
    }

    public CatalogKey catalogKeyGet() {
        return new CatalogKey(this.catalog);
    }

    public String toPrivilegeString() {
        StringBuffer buffer = new StringBuffer();
        if (this.catalog != null) {
            buffer.append("`");
            buffer.append(this.catalog);
            buffer.append("`");
            buffer.append(".");
        }
        buffer.append("`");
        buffer.append(this.db);
        buffer.append("`");
        buffer.append(".");
        buffer.append("`");
        buffer.append(this.table);
        buffer.append("`");
        return buffer.toString();
    }

    public int hashCode() {
        int hashCode = 0;
        if (this.catalog != null)
            hashCode += this.catalog.hashCode();
        if (this.db != null)
            hashCode += this.db.hashCode();
        if (this.table != null)
            hashCode += this.table.hashCode();
        return hashCode;
    }

    public boolean equals(Object other) {
        if (this == other)
            return true;
        if (other == null)
            return false;
        if (other instanceof TableKey) {
            TableKey otherKey = (TableKey)other;
            if (this.catalog != null) {
                if (!this.catalog.equals(otherKey.getCatalog()))
                    return false;
            } else if (null != otherKey.getCatalog()) {
                return false;
            }
            if (this.db != null) {
                if (!this.db.equals(otherKey.getDb()))
                    return false;
            } else if (null != otherKey.getDb()) {
                return false;
            }
            if (this.table != null) {
                if (!this.table.equals(otherKey.getTable()))
                    return false;
            } else if (null != otherKey.getTable()) {
                return false;
            }
        } else {
            return false;
        }
        return true;
    }
}

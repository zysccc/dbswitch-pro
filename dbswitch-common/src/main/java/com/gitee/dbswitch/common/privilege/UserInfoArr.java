package com.gitee.dbswitch.common.privilege;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class UserInfoArr implements Serializable {
    private List<UserInfo> users;

    private boolean isGlobalGrant;

    public void setUsers(List<UserInfo> users) {
        this.users = users;
    }

    public void setGlobalGrant(boolean isGlobalGrant) {
        this.isGlobalGrant = isGlobalGrant;
    }



    protected boolean canEqual(Object other) {
        return other instanceof UserInfoArr;
    }


    public String toString() {
        return "UserInfoResponse(users=" + getUsers() + ", isGlobalGrant=" + isGlobalGrant() + ")";
    }

    public List<UserInfo> getUsers() {
        return this.users;
    }

    public boolean isGlobalGrant() {
        return this.isGlobalGrant;
    }

    public static class UserInfo implements Serializable {
        private String user;

        private String userName;

        private String host;

        private List<String> roles;

        private Date createTime;

        public void setUser(String user) {
            this.user = user;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public void setHost(String host) {
            this.host = host;
        }

        public void setRoles(List<String> roles) {
            this.roles = roles;
        }

        public void setCreateTime(Date createTime) {
            this.createTime = createTime;
        }



        protected boolean canEqual(Object other) {
            return other instanceof UserInfo;
        }

        public String toString() {
            return "UserInfoResponse.UserInfo(user=" + getUser() + ", userName=" + getUserName() + ", host=" + getHost() + ", roles=" + getRoles() + ", createTime=" + getCreateTime() + ")";
        }

        public String getUser() {
            return this.user;
        }

        public String getUserName() {
            return this.userName;
        }

        public String getHost() {
            return this.host;
        }

        public List<String> getRoles() {
            return this.roles;
        }

        public Date getCreateTime() {
            return this.createTime;
        }
    }
}

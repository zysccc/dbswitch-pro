package com.gitee.dbswitch.common.privilege;

import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

public class CatalogKey implements Serializable {
    private String catalog;

    public void setCatalog(String catalog) {
        this.catalog = catalog;
    }

    public String toString() {
        return "CatalogKey(catalog=" + getCatalog() + ")";
    }

    public CatalogKey(String catalog) {
        this.catalog = catalog;
    }

    public CatalogKey() {}

    public String getCatalog() {
        return this.catalog;
    }

    public void checkIllegal() throws Exception {
        if (StringUtils.isEmpty(this.catalog))
            throw new Exception("the catalog name is empty.");
    }

    public String toPrivilegeString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append("`");
        buffer.append(this.catalog);
        buffer.append("`");
        buffer.append(".*.*");
        return buffer.toString();
    }

    public int hashCode() {
        int hashCode = 0;
        if (this.catalog != null)
            hashCode += this.catalog.hashCode();
        return hashCode;
    }

    public boolean equals(Object other) {
        if (this == other)
            return true;
        if (other == null)
            return false;
        if (other instanceof CatalogKey) {
            CatalogKey otherKey = (CatalogKey)other;
            if (this.catalog != null) {
                if (!this.catalog.equals(otherKey.getCatalog()))
                    return false;
            } else if (null != otherKey.getCatalog()) {
                return false;
            }
        } else {
            return false;
        }
        return true;
    }
}


package com.gitee.dbswitch.common.privilege;


import com.google.common.collect.Sets;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

public class CreateUserRequest implements Serializable {
    public void setName(String name) {
        this.name = name;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setRoles(Set<String> roles) {
        this.roles = roles;
    }

    public void setAddPrivilege(PrivilegeRequest addPrivilege) {
        this.addPrivilege = addPrivilege;
    }

    public void setRevokePrivilege(PrivilegeRequest revokePrivilege) {
        this.revokePrivilege = revokePrivilege;
    }

    protected boolean canEqual(Object other) {
        return other instanceof CreateUserRequest;
    }


    public String toString() {
        return "CreateUserRequest(name=" + getName() + ", host=" + getHost() + ", password=" + getPassword() + ", roles=" + getRoles() + ", addPrivilege=" + getAddPrivilege() + ", revokePrivilege=" + getRevokePrivilege() + ")";
    }

    private static final Logger log = LoggerFactory.getLogger(CreateUserRequest.class);

    private String name;

    private String host;

    private String password;

    public String getName() {
        return this.name;
    }

    public String getHost() {
        return this.host;
    }

    public String getPassword() {
        return this.password;
    }

    private Set<String> roles = Sets.newHashSet();

    private PrivilegeRequest addPrivilege;

    private PrivilegeRequest revokePrivilege;

    public Set<String> getRoles() {
        return this.roles;
    }

    public PrivilegeRequest getAddPrivilege() {
        return this.addPrivilege;
    }

    public PrivilegeRequest getRevokePrivilege() {
        return this.revokePrivilege;
    }

    public void checkIllegal() throws Exception {
        if (StringUtils.isEmpty(this.name) || StringUtils.isEmpty(this.password))
            throw new Exception("The name or password is empty.");
        if (Objects.nonNull(this.addPrivilege))
            this.addPrivilege.checkIllegal();
        if (Objects.nonNull(this.revokePrivilege))
            this.revokePrivilege.checkIllegal();
    }
}


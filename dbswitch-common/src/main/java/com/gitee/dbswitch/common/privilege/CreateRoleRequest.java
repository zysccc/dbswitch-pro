package com.gitee.dbswitch.common.privilege;

import java.io.Serializable;

public class CreateRoleRequest implements Serializable {
    private String name;

    private PrivilegeRequest addPrivilege;

    private PrivilegeRequest revokePrivilege;

    public void setName(String name) {
        this.name = name;
    }

    public void setAddPrivilege(PrivilegeRequest addPrivilege) {
        this.addPrivilege = addPrivilege;
    }

    public void setRevokePrivilege(PrivilegeRequest revokePrivilege) {
        this.revokePrivilege = revokePrivilege;
    }


    protected boolean canEqual(Object other) {
        return other instanceof CreateRoleRequest;
    }


    public String toString() {
        return "CreateRoleRequest(name=" + getName() + ", addPrivilege=" + getAddPrivilege() + ", revokePrivilege=" + getRevokePrivilege() + ")";
    }

    public String getName() {
        return this.name;
    }

    public PrivilegeRequest getAddPrivilege() {
        return this.addPrivilege;
    }

    public PrivilegeRequest getRevokePrivilege() {
        return this.revokePrivilege;
    }
}


package com.gitee.dbswitch.common.privilege;

import com.google.common.collect.Sets;

import java.io.Serializable;
import java.util.Set;

public class UpdataUserRequest implements Serializable {
    private String name;

    private String password;

    public void setName(String name) {
        this.name = name;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setOldRoles(Set<String> oldRoles) {
        this.oldRoles = oldRoles;
    }

    public void setNewRoles(Set<String> newRoles) {
        this.newRoles = newRoles;
    }


    protected boolean canEqual(Object other) {
        return other instanceof UpdataUserRequest;
    }


    public String toString() {
        return "UpdataUserRequest(name=" + getName() + ", password=" + getPassword() + ", oldRoles=" + getOldRoles() + ", newRoles=" + getNewRoles() + ")";
    }

    public String getName() {
        return this.name;
    }

    public String getPassword() {
        return this.password;
    }

    private Set<String> oldRoles = Sets.newHashSet();

    public Set<String> getOldRoles() {
        return this.oldRoles;
    }

    private Set<String> newRoles = Sets.newHashSet();

    public Set<String> getNewRoles() {
        return this.newRoles;
    }
}


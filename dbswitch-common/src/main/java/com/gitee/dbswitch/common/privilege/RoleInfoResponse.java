package com.gitee.dbswitch.common.privilege;

import java.io.Serializable;
import java.util.Date;

public class RoleInfoResponse implements Serializable {
    private String name;

    private String users;

    private Date createTime;

    public void setName(String name) {
        this.name = name;
    }

    public void setUsers(String users) {
        this.users = users;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }


    protected boolean canEqual(Object other) {
        return other instanceof RoleInfoResponse;
    }


    public String toString() {
        return "RoleInfoResponse(name=" + getName() + ", users=" + getUsers() + ", createTime=" + getCreateTime() + ")";
    }

    public String getName() {
        return this.name;
    }

    public String getUsers() {
        return this.users;
    }

    public Date getCreateTime() {
        return this.createTime;
    }
}

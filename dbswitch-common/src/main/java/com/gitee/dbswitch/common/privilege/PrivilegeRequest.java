package com.gitee.dbswitch.common.privilege;

import org.springframework.util.CollectionUtils;

import java.io.Serializable;
import java.util.List;

public class PrivilegeRequest implements Serializable {
    private String globals;

    private List<CatalogPrivilege> catalogs;

    private List<DbPrivilege> databases;

    private List<TablePrivilege> tables;

    private List<ResourcePrivilege> resources;

    public void setGlobals(String globals) {
        this.globals = globals;
    }

    public void setCatalogs(List<CatalogPrivilege> catalogs) {
        this.catalogs = catalogs;
    }

    public void setDatabases(List<DbPrivilege> databases) {
        this.databases = databases;
    }

    public void setTables(List<TablePrivilege> tables) {
        this.tables = tables;
    }

    public void setResources(List<ResourcePrivilege> resources) {
        this.resources = resources;
    }


    public String toString() {
        return "PrivilegeRequest(globals=" + getGlobals() + ", catalogs=" + getCatalogs() + ", databases=" + getDatabases() + ", tables=" + getTables() + ", resources=" + getResources() + ")";
    }

    public String getGlobals() {
        return this.globals;
    }

    public List<CatalogPrivilege> getCatalogs() {
        return this.catalogs;
    }

    public List<DbPrivilege> getDatabases() {
        return this.databases;
    }

    public List<TablePrivilege> getTables() {
        return this.tables;
    }

    public List<ResourcePrivilege> getResources() {
        return this.resources;
    }

    public void checkIllegal() throws Exception {
        if (!CollectionUtils.isEmpty(this.catalogs))
            for (CatalogPrivilege catalogPrivilege : this.catalogs)
                catalogPrivilege.checkIllegal();
        if (!CollectionUtils.isEmpty(this.databases))
            for (DbPrivilege dbPrivilege : this.databases)
                dbPrivilege.checkIllegal();
        if (!CollectionUtils.isEmpty(this.tables))
            for (TablePrivilege tablePrivilege : this.tables)
                tablePrivilege.checkIllegal();
        if (!CollectionUtils.isEmpty(this.resources))
            for (ResourcePrivilege resourcePrivilege : this.resources)
                resourcePrivilege.checkIllegal();
    }
}

package com.gitee.dbswitch.common.privilege;

import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.Objects;

public class CatalogPrivilege implements Serializable {
    private CatalogKey key;

    private String privilege;

    public void setKey(CatalogKey key) {
        this.key = key;
    }

    public void setPrivilege(String privilege) {
        this.privilege = privilege;
    }


    protected boolean canEqual(Object other) {
        return other instanceof CatalogPrivilege;
    }

    public String toString() {
        return "CatalogPrivilege(key=" + getKey() + ", privilege=" + getPrivilege() + ")";
    }

    public CatalogPrivilege(CatalogKey key, String privilege) {
        this.key = key;
        this.privilege = privilege;
    }

    public CatalogPrivilege() {}

    public CatalogKey getKey() {
        return this.key;
    }

    public String getPrivilege() {
        return this.privilege;
    }

    public void checkIllegal() throws Exception {
        if (StringUtils.isEmpty(this.privilege) || Objects.isNull(this.key))
            throw new Exception("the catalog key or privilege is empty.");
        this.key.checkIllegal();
    }
}

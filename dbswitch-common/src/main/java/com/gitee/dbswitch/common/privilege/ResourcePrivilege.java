package com.gitee.dbswitch.common.privilege;

import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

public class ResourcePrivilege implements Serializable {
    private String resource;

    private String privilege;

    public void setResource(String resource) {
        this.resource = resource;
    }

    public void setPrivilege(String privilege) {
        this.privilege = privilege;
    }


    protected boolean canEqual(Object other) {
        return other instanceof ResourcePrivilege;
    }


    public String toString() {
        return "ResourcePrivilege(resource=" + getResource() + ", privilege=" + getPrivilege() + ")";
    }

    public ResourcePrivilege(String resource, String privilege) {
        this.resource = resource;
        this.privilege = privilege;
    }

    public ResourcePrivilege() {}

    public String getResource() {
        return this.resource;
    }

    public String getPrivilege() {
        return this.privilege;
    }

    public void checkIllegal() throws Exception {
        if (StringUtils.isEmpty(this.privilege) || StringUtils.isEmpty(this.resource))
            throw new Exception("the resource or privilege is empty.");
    }
}

USE `dbswitch`;

insert into sys_user values(3,  105, 'dev1',    'dev1', '00', 'ry@qq.com',  '15666666666', '1', '', '$2a$10$zLiwJiBCJDhFODIVa6FadODrO3RPzs1o6Y4RfsNuaEt6kinG24JPu', '0', '0', '127.0.0.1', sysdate(), 'admin', sysdate(), '', null, '测试员');


insert into sys_role values('3', '开发角色',    'dev', 2, 2, 1, 1, '0', '0', 'admin', sysdate(), '', null, '开发角色');


insert into sys_menu values('2000', '数据库创建用户', '100', '1',  '', '', '', '', 1, 0, 'F', '0', '0', 'privilege:user:create', '#', 'admin', sysdate(), '', null, '数据库创建用户');
insert into sys_menu values('2001', '数据库删除用户', '100', '1',  '', '', '', '', 1, 0, 'F', '0', '0', 'privilege:user:delete', '#', 'admin', sysdate(), '', null, '数据库删除用户');
insert into sys_menu values('2002', '数据库用户授权', '100', '1',  '', '', '', '', 1, 0, 'F', '0', '0', 'privilege:user:add', '#', 'admin', sysdate(), '', null, '数据库用户授权');
insert into sys_menu values('3000', '表管理-list', '100', '1',  '', '', '', '', 1, 0, 'F', '0', '0', 'tb:manage:list', '#', 'admin', sysdate(), '', null, '数据库用户授权');


insert into sys_user_role values ('3', '3');

insert into sys_role_menu values ('3', '3000');
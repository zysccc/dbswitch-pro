USE `dbswitch`;

CREATE TABLE `zys_table_manage` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `database_id` bigint(20) DEFAULT NULL COMMENT '数据库实例id',
  `database_name` varchar(200) DEFAULT NULL COMMENT '数据库实例名',
  `dbname` varchar(200) NOT NULL DEFAULT '' COMMENT '注册表对应schema',
  `table_name` varchar(200) NOT NULL DEFAULT '' COMMENT '注册表名',
  `itrp` varchar(255) NOT NULL DEFAULT '' COMMENT 'it负责任',
  `itrp_dept` varchar(200) NOT NULL DEFAULT '' COMMENT 'it负责任部门',
  `owner` varchar(20) NOT NULL DEFAULT '' COMMENT '表主人',
  `owner_dept` varchar(200) NOT NULL DEFAULT '' COMMENT '表主人部门',
  `partition` varchar(200) NOT NULL DEFAULT '全量' COMMENT '分区：全量、日、周、月、key',
  `est_total_rows` bigint(20) DEFAULT NULL COMMENT '数据总行数',
  `est_daily_rows` bigint(20) DEFAULT NULL COMMENT '数据日增总行数',
  `data_freq` varchar(200) DEFAULT NULL COMMENT '数据更新频率：分钟、小时、日、周、月、年',
  `data_level` varchar(200) DEFAULT NULL COMMENT '数据等级：敏感、次敏感',
  `data_desc` varchar(500) DEFAULT NULL COMMENT '数据使用场景描述',
  `ddl` varchar(2000) NOT NULL DEFAULT '' COMMENT 'ddl',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='数据资产表'
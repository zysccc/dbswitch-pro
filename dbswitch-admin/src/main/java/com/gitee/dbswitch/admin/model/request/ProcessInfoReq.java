package com.gitee.dbswitch.admin.model.request;

import com.gitee.dbswitch.admin.entity.TableInfoEntity;
import com.gitee.dbswitch.common.privilege.PrivilegeRequest;
import com.gitee.dbswitch.common.type.ProcessTypeEnum;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("流程明细")
public class ProcessInfoReq {
    private Long id;
    private Long databaseId;
    private String processtype;
    private String username;
    private String processInfoJson;
    private TableInfoEntity tableInfo;
    private GrantReq grantReq;
    private String action;
    private Integer processStatus;
    private String remarks;
    private Timestamp createTime;
    private Timestamp updateTime;
}

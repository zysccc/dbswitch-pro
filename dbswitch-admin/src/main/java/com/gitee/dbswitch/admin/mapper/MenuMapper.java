package com.gitee.dbswitch.admin.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gitee.dbswitch.admin.entity.Menu;

public interface MenuMapper extends BaseMapper<Menu> {

}
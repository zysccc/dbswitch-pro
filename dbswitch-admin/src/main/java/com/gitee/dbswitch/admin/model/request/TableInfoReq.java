package com.gitee.dbswitch.admin.model.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TableInfoReq {
    private Long id;
    private Long databaseId;
    private Long databaseName;
    private String dbname;
    private String tableName;
    private String itrp;
    private String itrpDept;
    private String owner;
    private String ownerDept;
    private String partition;
    private Long estTotalRows;
    private Long estDailyRows;
    private String dataFreq;
    private String dataLevel;
    private String dataDesc;
    private String ddl;
    private Timestamp createTime;
    private Timestamp updateTime;
}

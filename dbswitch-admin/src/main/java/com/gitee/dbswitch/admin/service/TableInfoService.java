package com.gitee.dbswitch.admin.service;

import cn.hutool.core.bean.BeanUtil;
import com.gitee.dbswitch.admin.common.response.PageResult;
import com.gitee.dbswitch.admin.common.response.Result;
import com.gitee.dbswitch.admin.dao.ProcessDao;
import com.gitee.dbswitch.admin.dao.TableInfoDao;
import com.gitee.dbswitch.admin.entity.ProcessInfoEntity;
import com.gitee.dbswitch.admin.entity.TableInfoEntity;
import com.gitee.dbswitch.admin.model.request.ProcessInfoReq;
import com.gitee.dbswitch.admin.model.request.ProcessInfoSearchReq;
import com.gitee.dbswitch.admin.model.request.TableInfoReq;
import com.gitee.dbswitch.admin.model.request.TableInfoSearchReq;
import com.gitee.dbswitch.admin.model.response.ProcessInfoResp;
import com.gitee.dbswitch.admin.model.response.TableInfoResp;
import com.gitee.dbswitch.admin.util.PageUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

@Slf4j
@Service
public class TableInfoService {

    @Resource
    private TableInfoDao tableInfoDao;
    @Resource
    private ProcessService processService;

    public PageResult<TableInfoResp> getTableInfoList(TableInfoSearchReq searchReq) {
        Supplier<List<TableInfoResp>> method = () -> {
            List<TableInfoEntity> list = tableInfoDao.listAll(searchReq.getSearchtablename(), searchReq.getOwner(),searchReq.getItrp(),searchReq.getDatabaseId());
            ArrayList<TableInfoResp> rs = new ArrayList<>();
            list.stream().forEach(c ->{
                TableInfoResp infoResp = new TableInfoResp();
                BeanUtil.copyProperties(c,infoResp);
                rs.add(infoResp);
            });
            return rs;
        };
        return PageUtils.getPage(method,searchReq.getPage(),searchReq.getSize());
    }

    public Result<String> saveTableInfo(TableInfoReq req) {
        TableInfoEntity tableInfoEntity = new TableInfoEntity();
        BeanUtil.copyProperties(req,tableInfoEntity);
        tableInfoDao.insert(tableInfoEntity);
        return Result.success("成功提交！");

    }

    public Result<String> updateTableInfo(TableInfoReq req) {
        TableInfoEntity tableInfoEntity = new TableInfoEntity();
        BeanUtil.copyProperties(req,tableInfoEntity);
        tableInfoDao.updateById(tableInfoEntity);
        return Result.success("成功更新！");

    }

    public Result<String> deleteTableInfo(TableInfoReq req) {
        TableInfoEntity tableInfoEntity = new TableInfoEntity();
        BeanUtil.copyProperties(req,tableInfoEntity);
        tableInfoDao.deleteById(tableInfoEntity.getId());
        return Result.success("成功删除！");
    }
}

package com.gitee.dbswitch.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gitee.dbswitch.admin.entity.ProcessInfoEntity;
import com.gitee.dbswitch.admin.entity.TableInfoEntity;

public interface TableInfoMapper extends BaseMapper<TableInfoEntity> {

}

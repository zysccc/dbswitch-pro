package com.gitee.dbswitch.admin.service;

import cn.hutool.core.bean.BeanUtil;
import com.gitee.dbswitch.admin.entity.DatabaseConnectionEntity;
import com.gitee.dbswitch.admin.model.response.UserInfoResponse;
import com.gitee.dbswitch.common.privilege.*;
import com.gitee.dbswitch.service.MetadataService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class UserPrivilegeService {
    private static final String DEFAULT_CLUSTER_REG = "default_cluster:";

    private static final String ADMIN_ROLE_NAME = "admin";

    private static final String ROOT_USER_NAME = "root";

    private static final String ROOT_ROLE_NAME = "operator";


    @Resource
    private ConnectionService connectionService;

    @Resource
    private AdminConnService adminConnService;

    public UserInfoResponse getUsers(long id) throws Exception {
        DatabaseConnectionEntity connectionEntity = connectionService.getDatabaseConnectionById(id);
        MetadataService metaDataCoreService = connectionService.getMetaDataCoreService(id);
       // DatabaseConnectionEntity connectionEntity = adminConnService.getConn(1);
        //MetadataService adminMetadataService = connectionService.getAdminMetadataService(connectionEntity);

        UserInfoArr users = metaDataCoreService.getUsers(connectionEntity.getUsername());
        UserInfoResponse userInfoResponse = new UserInfoResponse();
        BeanUtil.copyProperties(users,userInfoResponse);
        return userInfoResponse;
    }
    public UserPrivilegeResponse getUserOrRolePrivilege(String user, String role,long id) throws Exception {
        DatabaseConnectionEntity connectionEntity = connectionService.getDatabaseConnectionById(id);
        MetadataService metaDataCoreService = connectionService.getMetaDataCoreService(id);
        UserPrivilege userOrRolePrivilege = metaDataCoreService.getUserOrRolePrivilege(user, role);
        UserPrivilegeResponse userPrivilegeResponse = new UserPrivilegeResponse();
        BeanUtil.copyProperties(userOrRolePrivilege,userPrivilegeResponse);
        return userPrivilegeResponse;
    }

    public Boolean addUserOrRolePrivilege(Long id,PrivilegeRequest request, String user, String role) throws Exception {
        MetadataService metaDataCoreService = connectionService.getMetaDataCoreService(id);
       return metaDataCoreService.addUserOrRolePrivilege(request,user,role);

    }
    public Boolean revokeUserOrRolePrivilege(Long id,PrivilegeRequest request, String user, String role) throws Exception {
        MetadataService metaDataCoreService = connectionService.getMetaDataCoreService(id);
        return metaDataCoreService.revokeUserOrRolePrivilege(request,user,role);
    }

    public Boolean createUser(CreateUserRequest request,long id) throws Exception {
        MetadataService metaDataCoreService = connectionService.getMetaDataCoreService(id);
        return metaDataCoreService.createUser(request);
    }
    public Boolean updateUser(UpdataUserRequest request,long id) throws Exception {
        MetadataService metaDataCoreService = connectionService.getMetaDataCoreService(id);
        return metaDataCoreService.updateUser(request);

    }
    public Boolean dropUser(String user,long id) throws Exception {
        MetadataService metaDataCoreService = connectionService.getMetaDataCoreService(id);
        return metaDataCoreService.dropUser(user);
    }
    public Boolean exeDdl(String ddl,long id) throws Exception {
        MetadataService metaDataCoreService = connectionService.getMetaDataCoreService(id);
        return metaDataCoreService.exeDdl(ddl);
    }
    public Boolean checkDdl(String ddl,long id) throws Exception {
        MetadataService metaDataCoreService = connectionService.getMetaDataCoreService(id);
        return metaDataCoreService.checkDdl(ddl);
    }

}

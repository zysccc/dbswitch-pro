package com.gitee.dbswitch.admin.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@TableName("sys_role_menu")
public class RoleMenu {
    /**
     * 表：sys_role_menu
     * 字段：id
     * 注释：
     *
     * @mbggenerated
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 表：sys_role_menu
     * 字段：role_id
     * 注释：角色ID
     *
     * @mbggenerated
     */
    @TableField("role_id")
    private Integer roleId;

    /**
     * 表：sys_role_menu
     * 字段：menu_code
     * 注释：菜单ID
     *
     * @mbggenerated
     */
    @TableField("menu_code")
    private String menuCode;

}
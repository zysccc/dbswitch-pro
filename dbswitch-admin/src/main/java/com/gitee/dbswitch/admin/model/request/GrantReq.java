package com.gitee.dbswitch.admin.model.request;

import com.gitee.dbswitch.common.privilege.CatalogPrivilege;
import com.gitee.dbswitch.common.privilege.DbPrivilege;
import com.gitee.dbswitch.common.privilege.TablePrivilege;
import lombok.Data;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

@Data
public class GrantReq {
    private Long databaseId;
    private String username;
    private List<CatalogPrivilege> catalogs= new ArrayList<>();
    private List<DbPrivilege> databases=new ArrayList<>();
    private List<TablePrivilege> tables;
}

package com.gitee.dbswitch.admin.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gitee.dbswitch.admin.entity.Role;

public interface RoleMapper extends BaseMapper<Role> {

}
package com.gitee.dbswitch.admin.security57.controller;


import com.gitee.dbswitch.admin.security57.model.AjaxResult;
import com.gitee.dbswitch.admin.security57.model.RegisterBody;
import com.gitee.dbswitch.admin.security57.service.SysRegisterService;
import com.gitee.dbswitch.admin.security57.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 注册验证
 * 
 * @author ruoyi
 */
@RestController
public class SysRegisterController
{
    @Autowired
    private SysRegisterService registerService;


    @PostMapping("/register")
    public AjaxResult register(@RequestBody RegisterBody user)
    {
        String msg = registerService.register(user);
        return StringUtils.isEmpty(msg) ? AjaxResult.success() : AjaxResult.error(msg);
    }
}

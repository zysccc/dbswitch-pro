package com.gitee.dbswitch.admin.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.gitee.dbswitch.admin.entity.Menu;
import com.gitee.dbswitch.admin.entity.User;
import com.gitee.dbswitch.admin.mapper.MenuMapper;
import com.gitee.dbswitch.admin.mapper.UserMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

@Repository
public class MenuDao {
    @Resource
    private MenuMapper menuMapper;

    public int deleteByPrimaryKey(String code){
        int i = menuMapper.deleteById(code);
        return i;
    }

    public int insert(Menu record){
        int insert = menuMapper.insert(record);
        return insert;
    }

    public int insertSelective(Menu record){
        return menuMapper.insert(record);
    }

    public Menu selectByPrimaryKey(String code){
        return menuMapper.selectById(code);
    }

    public int updateByPrimaryKeySelective(Menu record){
        return menuMapper.updateById(record);
    }

    public int updateByPrimaryKey(Menu record){
        return menuMapper.updateById(record);
    }
}

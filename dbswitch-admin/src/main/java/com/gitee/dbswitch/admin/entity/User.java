package com.gitee.dbswitch.admin.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@TableName("sys_user")
public class User {
    /**
     * 表：sys_user
     * 字段：id
     * 注释：系统用户主键
     *
     * @mbggenerated
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 表：sys_user
     * 字段：username
     * 注释：系统用户登陆账号
     *
     * @mbggenerated
     */
    @TableField("username")
    private String username;

    /**
     * 表：sys_user
     * 字段：password
     * 注释：系统用户登陆密码
     *
     * @mbggenerated
     */
    @TableField("password")
    private String password;

    /**
     * 表：sys_user
     * 字段：full_name
     * 注释：系统用户全称
     *
     * @mbggenerated
     */
    @TableField("full_name")
    private String fullName;

    /**
     * 表：sys_user
     * 字段：enabled
     * 注释：系统用户启用（0-否，1-是）
     *
     * @mbggenerated
     */
    @TableField("enabled")
    private Boolean enabled;

    /**
     * 表：sys_user
     * 字段：account_non_locked
     * 注释：账户非锁定（0-否，1-是）
     *
     * @mbggenerated
     */
    @TableField("account_non_locked")
    private Boolean accountNonLocked;
    

    
}
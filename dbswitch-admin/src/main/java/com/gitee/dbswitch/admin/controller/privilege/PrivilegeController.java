package com.gitee.dbswitch.admin.controller.privilege;

import com.gitee.dbswitch.admin.common.response.Result;
import com.gitee.dbswitch.admin.config.SwaggerConfig;
import com.gitee.dbswitch.admin.model.response.UserInfoResponse;
import com.gitee.dbswitch.admin.service.UserPrivilegeService;
import com.gitee.dbswitch.common.privilege.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@Api(tags = {"权限查询接口"})
@RestController
@RequestMapping(value = SwaggerConfig.API_V1 + "/privilege")
public class PrivilegeController {

    @Resource
    private UserPrivilegeService userPrivilegeService;

    //todo
    @ApiOperation("get all user name list")
    @GetMapping(value = {"/user-name/list"}, produces = {"application/json"})
    public Result<List<Object>> getUserNameList() throws Exception {
        return null;
    }

    @ApiOperation("get all user list")
    @GetMapping(value = {"/user/list"}, produces = {"application/json"})
    public Result<UserInfoResponse> getUserList(long id) throws Exception {
        return Result.success(this.userPrivilegeService.getUsers(id));
    }
    @ApiOperation("create a user")
    @PreAuthorize("@ss.hasPermi('privilege:user:create')")
    @PostMapping(value = {"/user/create"}, produces = {"application/json"})
    public Result<Boolean> createUser(@RequestBody CreateUserRequest request,long id) throws Exception {
        userPrivilegeService.createUser(request,id);
        return Result.success(true);
    }
    @ApiOperation("update user password or role list")
    @PostMapping(value = {"/user/update"}, produces = {"application/json"})
    public Result<Boolean> updateUser(@RequestBody UpdataUserRequest request,long id) throws Exception {
        userPrivilegeService.updateUser(request,id);
        return Result.success(true);
    }
    @ApiOperation("delete a user")
    @DeleteMapping(value = {"/user/delete"}, produces = {"application/json"})
    public Result<Boolean> deleteUser(String user,long id) throws Exception {
        userPrivilegeService.dropUser(user,id);
        return Result.success(true);
    }

    @ApiOperation("get user or role privilege")
    @GetMapping(value = {"/list"}, produces = {"application/json"})
    public Result<UserPrivilegeResponse> getUserOrRolePrivilege(String user, String role,long id) throws Exception {
        return Result.success(this.userPrivilegeService.getUserOrRolePrivilege(user, role,id));
    }

    @ApiOperation("add user or role privilege")
    @PostMapping(value = {"/add"}, produces = {"application/json"})
    public Result<Boolean> addUserOrRolePrivilege(@RequestBody PrivilegeRequest request,Long id, String user, String role) throws Exception {
        return Result.success(this.userPrivilegeService.addUserOrRolePrivilege(id,request, user, role));
    }

    @ApiOperation("revoke user or role privilege")
    @PostMapping(value = {"/revoke"}, produces = {"application/json"})
    public Result<Boolean> revokeUserOrRolePrivilege(@RequestBody PrivilegeRequest request ,@PathVariable("id") Long id,String user, String role) throws Exception {
        return Result.success(this.userPrivilegeService.revokeUserOrRolePrivilege(id,request, user, role));
    }
    //todo
    @ApiOperation("create a role")
    @PostMapping(value = {"/role/create"}, produces = {"application/json"})
    public Result<Boolean> createRole(@RequestBody CreateRoleRequest request) throws Exception {
        return Result.success(true);
    }

    //todo
    @ApiOperation("get role list")
    @GetMapping(value = {"/role/list"}, produces = {"application/json"})
    public Result<List<RoleInfoResponse>> getRoles() throws Exception {
        return Result.success(true);
    }
    //todo
    @ApiOperation("get role list")
    @DeleteMapping(value = {"/role/delete"}, produces = {"application/json"})
    public Result<Boolean> dropRole(String role) throws Exception {
        return Result.success(true);
    }
}

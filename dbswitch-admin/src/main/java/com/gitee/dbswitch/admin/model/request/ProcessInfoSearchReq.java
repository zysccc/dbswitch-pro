package com.gitee.dbswitch.admin.model.request;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class ProcessInfoSearchReq {
    private String searchText;
    private String username;
    private Integer status;
    private Integer page;
    private Integer size;
}

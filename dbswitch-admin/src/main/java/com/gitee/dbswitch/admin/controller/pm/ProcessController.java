package com.gitee.dbswitch.admin.controller.pm;

import com.gitee.dbswitch.admin.common.response.PageResult;
import com.gitee.dbswitch.admin.common.response.Result;
import com.gitee.dbswitch.admin.config.SwaggerConfig;
import com.gitee.dbswitch.admin.model.request.ProcessInfoReq;
import com.gitee.dbswitch.admin.model.request.ProcessInfoSearchReq;
import com.gitee.dbswitch.admin.model.response.ProcessInfoResp;
import com.gitee.dbswitch.admin.service.ProcessService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@Api(tags = {"流程处理接口"})
@Slf4j
@RestController
@RequestMapping(value = SwaggerConfig.API_V1 + "/process")
public class ProcessController {

    @Resource
    private ProcessService processService;

    @ApiOperation("get all process")
    @GetMapping(value = {"/list"}, produces = {"application/json"})
    public PageResult<ProcessInfoResp> getPorcessList(@RequestBody ProcessInfoSearchReq searchReq)  {

        return processService.getProcessList(searchReq);
    }


    @ApiOperation("tempadd or add process")
    @PostMapping(value = {"/add"}, produces = {"application/json"})
    public Result<String> addPorcess(@RequestBody ProcessInfoReq req) throws Exception {
        log.info("请求参数:{}",req);
         return processService.saveProcessInfo(req);
//        else {
//            return processService.updateProcess(req);
//        }
    }


    @ApiOperation("update process")
    @PostMapping(value = {"/update"}, produces = {"application/json"})
    public Result<String> updatePorcess(@RequestBody ProcessInfoReq req)  {
        log.info("请求参数:{}",req);
        return processService.updateProcess(req);
    }

    @ApiOperation("delete process")
    @PostMapping(value = {"/del"}, produces = {"application/json"})
    public Result<String> deletePorcess(@RequestBody ProcessInfoReq req)  {
        log.info("请求参数:{}",req);
        return processService.deleteProcess(req);
    }

}

package com.gitee.dbswitch.admin.dao;

import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.gitee.dbswitch.admin.entity.DatabaseConnectionEntity;
import com.gitee.dbswitch.admin.entity.RoleMenu;
import com.gitee.dbswitch.admin.mapper.RoleMenuMapper;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

@Repository
public class RoleMenuDao {
    @Resource
    private RoleMenuMapper roleMenuMapper;

    public List<RoleMenu> selectByRoleId(Integer roleId){
        return roleMenuMapper.selectList(Wrappers.<RoleMenu>lambdaQuery().eq(ObjectUtils.isNotEmpty(roleId),RoleMenu::getRoleId,roleId));
    }

    public int deleteByPrimaryKey(Integer id){
        return roleMenuMapper.deleteById(id);
    }

    public int insertSelective(RoleMenu record){
        return roleMenuMapper.insert(record);
    }

    public RoleMenu selectByPrimaryKey(Integer id){
        return roleMenuMapper.selectById(id);
    }

    public int updateByPrimaryKeySelective(RoleMenu record){
        return roleMenuMapper.updateById(record);
    }
}

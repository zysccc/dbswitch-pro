package com.gitee.dbswitch.admin.model.response;

import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gitee.dbswitch.common.type.ProcessTypeEnum;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.ibatis.type.EnumTypeHandler;

import java.sql.Timestamp;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("流程明细")
public class ProcessInfoResp {
    private Long id;
    private String processtype;

    private String username;

    private String processInfoJson;

    private Integer processStatus;

    private String remarks;

    private Timestamp createTime;

    private Timestamp updateTime;
}

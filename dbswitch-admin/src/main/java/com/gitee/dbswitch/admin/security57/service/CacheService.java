package com.gitee.dbswitch.admin.security57.service;

import cn.hutool.cache.CacheUtil;
import cn.hutool.cache.impl.TimedCache;
import org.springframework.stereotype.Service;

@Service
public class CacheService {
    //创建缓存，默认20min过期
    TimedCache<String, Object> timedCache = CacheUtil.newTimedCache(1000*60*20);

    /**
     * 缓存基本的对象，Integer、String、实体类等
     *
     * @param key 缓存的键值
     * @param value 缓存的值
     */
    public <T> void setCacheObject(final String key, final T value)
    {
        timedCache.put(key,value);
    }

    /**
     * 缓存基本的对象，Integer、String、实体类等
     *
     * @param key 缓存的键值
     * @param value 缓存的值
     */
    public <T> void setCacheObject(final String key, final T value,long time)
    {
        timedCache.put(key,value,time);
    }
    /**
     * 获得缓存的基本对象。
     *
     * @param key 缓存键值
     * @return 缓存键值对应的数据
     */
    public <T> T getCacheObject(final String key)
    {
        Object o = timedCache.get(key);
        if(o==null){
            return null;
        }else {
            return (T)o;
        }

    }

    /**
     * 删除单个对象
     *
     * @param key
     */
    public boolean deleteObject(final String key)
    {
        timedCache.put(key,null,1);
        return true;
    }

}

package com.gitee.dbswitch.admin.service;

import com.gitee.dbswitch.admin.entity.DatabaseConnectionEntity;
import com.gitee.dbswitch.common.type.ProductTypeEnum;
import org.springframework.stereotype.Service;

@Service
public class AdminConnService {
    public DatabaseConnectionEntity getConn(int id){
        DatabaseConnectionEntity build = DatabaseConnectionEntity.builder().name("ds-admin").address("10.19.37.217").port("9030")
                .databaseName("ods").driver("com.mysql.jdbc.Driver").mode(0).type(ProductTypeEnum.DORIS)
                .url("jdbc:mysql://10.19.37.217:9030/test?useUnicode=true&characterEncoding=utf-8&useSSL=false&zeroDateTimeBehavior=convertToNull&serverTimezone=Asia/Shanghai&tinyInt1isBit=false&rewriteBatchedStatements=true&useCompression=true")
                .version("mysql-8").username("root2").password("Admin123").build();
        return build;
    }
}

package com.gitee.dbswitch.admin.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@TableName("sys_user_role")
public class UserRole {
    /**
     * 表：sys_user_role
     * 字段：id
     * 注释：主键
     *
     * @mbggenerated
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 表：sys_user_role
     * 字段：user_id
     * 注释：用户ID
     *
     * @mbggenerated
     */
    @TableField("user_id")
    private Integer userId;

    /**
     * 表：sys_user_role
     * 字段：role_id
     * 注释：角色ID
     *
     * @mbggenerated
     */
    @TableField("role_id")
    private Integer roleId;

}
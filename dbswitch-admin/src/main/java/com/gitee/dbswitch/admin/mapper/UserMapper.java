package com.gitee.dbswitch.admin.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gitee.dbswitch.admin.entity.User;

public interface UserMapper extends BaseMapper<User> {

}
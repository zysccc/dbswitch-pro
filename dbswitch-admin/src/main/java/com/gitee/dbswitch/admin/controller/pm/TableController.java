package com.gitee.dbswitch.admin.controller.pm;

import com.gitee.dbswitch.admin.common.response.PageResult;
import com.gitee.dbswitch.admin.common.response.Result;
import com.gitee.dbswitch.admin.config.SwaggerConfig;
import com.gitee.dbswitch.admin.model.request.ProcessInfoReq;
import com.gitee.dbswitch.admin.model.request.ProcessInfoSearchReq;
import com.gitee.dbswitch.admin.model.request.TableInfoReq;
import com.gitee.dbswitch.admin.model.request.TableInfoSearchReq;
import com.gitee.dbswitch.admin.model.response.ProcessInfoResp;
import com.gitee.dbswitch.admin.model.response.TableInfoResp;
import com.gitee.dbswitch.admin.service.ProcessService;
import com.gitee.dbswitch.admin.service.TableInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@Api(tags = {"表管理接口"})
@Slf4j
@RestController
@RequestMapping(value = SwaggerConfig.API_V1 + "/tb")
public class TableController {

    @Resource
    private TableInfoService tableInfoService;

    @ApiOperation("列出表信息")
    @PostMapping(value = {"/list"}, produces = {"application/json"})
    public PageResult<TableInfoResp> getPorcessList(@RequestBody TableInfoSearchReq searchReq) {
        return tableInfoService.getTableInfoList(searchReq);
    }

    @ApiOperation("新增表")
    @PostMapping(value = {"/add"}, produces = {"application/json"})
    public Result<String> addTableInfo(@RequestBody TableInfoReq req) {
        log.info("请求参数:{}", req);
        return tableInfoService.saveTableInfo(req);
    }

    @ApiOperation("更新表信息")
    @PostMapping(value = {"/update"}, produces = {"application/json"})
    public Result<String> updatePorcess(@RequestBody TableInfoReq req) {
        log.info("请求参数:{}", req);
        return tableInfoService.updateTableInfo(req);
    }

    @ApiOperation("删除表信息")
    @PreAuthorize("@ss.hasPermi('system:user:list')")
    @PostMapping(value = {"/del"}, produces = {"application/json"})
    public Result<String> deletePorcess(@RequestBody TableInfoReq req) {
        log.info("请求参数:{}", req);
        return tableInfoService.deleteTableInfo(req);
    }
}

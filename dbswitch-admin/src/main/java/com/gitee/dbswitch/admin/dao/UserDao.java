package com.gitee.dbswitch.admin.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.gitee.dbswitch.admin.entity.DatabaseConnectionEntity;
import com.gitee.dbswitch.admin.entity.User;
import com.gitee.dbswitch.admin.mapper.UserMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

@Repository
public class UserDao {
    @Resource
    private UserMapper userMapper;
    public int deleteByPrimaryKey(Integer id){
        int i = userMapper.deleteById(id);
        return i;

    }
    public int insertSelective(User record){
        int insert = userMapper.insert(record);
        return insert;
    }

    public User selectByPrimaryKey(Integer id){
        User user = userMapper.selectById(id);
        return user;
    }

    public int updateByPrimaryKeySelective(User record){
        int update = userMapper.updateById(record);
        return update;
    }

    public User selectByUsername(String username){
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(User::getUsername,username);
        return userMapper.selectOne(queryWrapper);
    }
}

package com.gitee.dbswitch.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gitee.dbswitch.admin.entity.ProcessInfoEntity;


public interface ProcessMapper extends BaseMapper<ProcessInfoEntity> {

}

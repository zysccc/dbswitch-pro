package com.gitee.dbswitch.admin.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@TableName("sys_menu")
public class Menu {
    /**
     * 表：sys_menu
     * 字段：code
     * 注释：主键ID
     *
     * @mbggenerated
     */
    @TableId(value = "code", type = IdType.AUTO)
    private String code;

    /**
     * 表：sys_menu
     * 字段：name
     * 注释：菜单名称
     *
     * @mbggenerated
     */
    @TableField(value = "name")
    private String name;

    /**
     * 表：sys_menu
     * 字段：parent_id
     * 注释：父ID
     *
     * @mbggenerated
     */
    @TableField(value = "parent_id")
    private String parentId;

    /**
     * 表：sys_menu
     * 字段：url
     * 注释：菜单链接
     *
     * @mbggenerated
     */
    @TableField(value = "url")
    private String url;

    /**
     * 表：sys_menu
     * 字段：type
     * 注释：类型 0：目录 1：菜单 2：按钮
     *
     * @mbggenerated
     */
    @TableField(value = "type")
    private Byte type;

    /**
     * 表：sys_menu
     * 字段：order_num
     * 注释：排序
     *
     * @mbggenerated
     */
    @TableField(value = "order_num")
    private Integer orderNum;

}
package com.gitee.dbswitch.admin.service;

import cn.hutool.core.bean.BeanUtil;
import com.gitee.dbswitch.admin.common.response.PageResult;
import com.gitee.dbswitch.admin.common.response.Result;
import com.gitee.dbswitch.admin.dao.AssignmentTaskDAO;
import com.gitee.dbswitch.admin.dao.ProcessDao;
import com.gitee.dbswitch.admin.dao.TableInfoDao;
import com.gitee.dbswitch.admin.entity.DatabaseConnectionEntity;
import com.gitee.dbswitch.admin.entity.ProcessInfoEntity;
import com.gitee.dbswitch.admin.entity.TableInfoEntity;
import com.gitee.dbswitch.admin.model.request.GrantReq;
import com.gitee.dbswitch.admin.model.request.ProcessInfoReq;
import com.gitee.dbswitch.admin.model.request.ProcessInfoSearchReq;
import com.gitee.dbswitch.admin.model.request.TableInfoSearchReq;
import com.gitee.dbswitch.admin.model.response.DbConnectionNameResponse;
import com.gitee.dbswitch.admin.model.response.ProcResultResp;
import com.gitee.dbswitch.admin.model.response.ProcessInfoResp;
import com.gitee.dbswitch.admin.model.response.TableInfoResp;
import com.gitee.dbswitch.admin.util.PageUtils;
import com.gitee.dbswitch.common.privilege.PrivilegeRequest;
import com.gitee.dbswitch.common.privilege.TablePrivilege;
import com.gitee.dbswitch.data.util.JsonUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;

@Slf4j
@Service
public class ProcessService {
    private final static String ACTION_REQ = "REQ";
    private final static String ACTION_FIN = "FIN";
    private final static String PROCTYPE_T = "T";
    private final static String PROCTYPE_G = "G";
    private final static int PROCSTAT_0 = 0;//暂存
    private final static int PROCSTAT_1 = 1;//提交
    private final static int PROCSTAT_2 = 2;//审批
    private final static int PROCSTAT_3 = 3;//审批成功
    private final static int PROCSTAT_4 = 4;//驳回修改


    @Resource
    private ProcessDao processDao;
    @Resource
    private TableInfoService tableInfoService;
    @Resource
    private TableInfoDao tableInfoDao;
    @Resource
    private UserPrivilegeService userPrivilegeService;

    public PageResult<ProcessInfoResp> getProcessList(ProcessInfoSearchReq searchReq) {
        Supplier<List<ProcessInfoResp>> method = () -> {
            List<ProcessInfoEntity> list = processDao.listAll(searchReq.getSearchText(),
                    searchReq.getStatus(), searchReq.getUsername());
            ArrayList<ProcessInfoResp> rs = new ArrayList<>();
            list.stream().forEach(c -> {
                ProcessInfoResp infoResp = new ProcessInfoResp();
                BeanUtil.copyProperties(c, infoResp);
                rs.add(infoResp);
            });
            return rs;
        };

        return PageUtils.getPage(method, searchReq.getPage(), searchReq.getSize());
    }

    public Result<String> saveProcessInfo(ProcessInfoReq req) throws Exception {
        ProcessInfoEntity processInfoEntity = new ProcessInfoEntity();
        BeanUtil.copyProperties(req, processInfoEntity);
        if(PROCTYPE_T.equals(req.getProcesstype())){
            processInfoEntity.setProcessInfoJson(JsonUtils.toJsonString(req.getTableInfo()));
        }else if(PROCTYPE_G.equals(req.getProcesstype())){
            processInfoEntity.setProcessInfoJson(JsonUtils.toJsonString(req.getGrantReq()));
        }
        boolean newproc=true;
        if(req.getId()!=null){
            ProcessInfoEntity entity1 = processDao.getById(req.getId());
            if(entity1!=null){
                newproc=false;
            }
        }
        ProcResultResp procResultResp=null;
        if (PROCSTAT_0 == req.getProcessStatus()) {
            procResultResp= handle0status(req, processInfoEntity, newproc);
        }
        if (PROCSTAT_1 == req.getProcessStatus()) {
            procResultResp = handle1status(req, processInfoEntity, newproc);
        }
        if (PROCSTAT_2 == req.getProcessStatus()) {
            procResultResp = handle2status(req, processInfoEntity, newproc);
        }
        if (PROCSTAT_3 == req.getProcessStatus()) {
            procResultResp= handle3status(req,processInfoEntity,newproc);
        }
        if (PROCSTAT_4 == req.getProcessStatus()) {
            procResultResp= handle4status(req,processInfoEntity,newproc);
        }
        if(newproc&&procResultResp.isStatus()){
            processDao.insert(processInfoEntity);
        }else if(!newproc&&procResultResp.isStatus()) {
            processDao.updateById(processInfoEntity);
        }
        return Result.success(procResultResp);

    }

    private ProcResultResp handle4status(ProcessInfoReq req, ProcessInfoEntity entity, boolean newproc) {
        entity.setProcessStatus(PROCSTAT_4);
        return ProcResultResp.builder().msg("驳回成功！").status(true).build();
    }

    public Result<String> updateProcess(ProcessInfoReq req) {
        ProcessInfoEntity processInfoEntity = new ProcessInfoEntity();
        BeanUtil.copyProperties(req, processInfoEntity);
        processDao.updateById(processInfoEntity);
        return Result.success("成功更新！");

    }

    public Result<String> deleteProcess(ProcessInfoReq req) {
        ProcessInfoEntity processInfoEntity = new ProcessInfoEntity();
        BeanUtil.copyProperties(req, processInfoEntity);
        processDao.deleteById(processInfoEntity.getId());
        return Result.success("成功删除！");
    }

    public void updateOrInsertProcessInfo(ProcessInfoEntity entity){
        ProcessInfoEntity entity1 = processDao.getById(entity.getId());
        if(entity1!=null){
            processDao.updateById(entity);
        }else {
            processDao.insert(entity);
        }
    }
    public ProcResultResp handle0status(ProcessInfoReq req, ProcessInfoEntity entity, Boolean newproc)throws Exception{
        //暂时什么都不做
        //updateOrInsertProcessInfo(entity);
        System.out.println("处理暂存请求");
        return ProcResultResp.builder().msg("暂存成功！").status(true).build();
    }
    public ProcResultResp handle1status(ProcessInfoReq req,ProcessInfoEntity entity,Boolean newproc) throws Exception {
        System.out.println("处理申请请求");
        //
        if(PROCTYPE_T.equals(req.getProcesstype())){
            //需要校验建表语句
            TableInfoEntity tableInfo = req.getTableInfo();
            String ddl = tableInfo.getDdl();
            if(userPrivilegeService.checkDdl(ddl, entity.getDatabaseId())){
                //updateOrInsertProcessInfo(entity);
                return ProcResultResp.builder().msg("申请提交成功！").status(true).build();
            }
        }else if(PROCTYPE_G.equals(req.getProcesstype())){
            //updateOrInsertProcessInfo(entity);
            //需要确认授权请求是否合理
            return ProcResultResp.builder().msg("申请提交成功！").status(true).build();
        }
        return null ;
    }
    public ProcResultResp handle2status(ProcessInfoReq req,ProcessInfoEntity entity,Boolean newproc) throws Exception {
        if(PROCTYPE_T.equals(req.getProcesstype())){
            //需要校验建表语句、请求建表请求合理性、自动建表
            TableInfoEntity tableInfo = req.getTableInfo();
            String ddl = tableInfo.getDdl();
            if(userPrivilegeService.checkDdl(ddl, entity.getDatabaseId())){
                TableInfoSearchReq tableInfoSearchReq = new TableInfoSearchReq();
                tableInfoSearchReq.setSearchtablename(tableInfo.getTableName());
                tableInfoSearchReq.setPage(1);
                tableInfoSearchReq.setSize(1);
                PageResult<TableInfoResp> rs = tableInfoService.getTableInfoList(tableInfoSearchReq);
                if(rs.getData()!=null&&rs.getData().size()>0){
                    return ProcResultResp.builder().msg("未创建，表存在").status(true).build();
                }else{
                    userPrivilegeService.exeDdl(tableInfo.getDdl(),entity.getDatabaseId());
                    tableInfoDao.insert(tableInfo);
                    entity.setProcessStatus(PROCSTAT_3);
                    return ProcResultResp.builder().msg("审批成功！").status(true).build();
                }
            }else {
                return ProcResultResp.builder().msg("未创建，校验不通过").status(false).build();
            }

        }else if(PROCTYPE_G.equals(req.getProcesstype())){
            //需要确认授权请求是否合理
            if(true){
                GrantReq grantReq = req.getGrantReq();
                PrivilegeRequest privilegeRequest = buildPrivilegeRequest(grantReq);
                Boolean aBoolean = userPrivilegeService.addUserOrRolePrivilege(req.getDatabaseId(), privilegeRequest, grantReq.getUsername(), null);
                entity.setProcessStatus(PROCSTAT_3);
                return ProcResultResp.builder().msg("审批成功！").status(aBoolean).build();
            }else {
                //entity.setProcessStatus(PROCSTAT_4);
                return ProcResultResp.builder().msg("审批失败！").status(true).build();
            }
        }
        return null ;
    }
    public ProcResultResp handle3status(ProcessInfoReq req,ProcessInfoEntity entity,Boolean newproc){
        return ProcResultResp.builder().msg("重复提交").status(true).build();
    }
    public PrivilegeRequest buildPrivilegeRequest(GrantReq grantReq){
        PrivilegeRequest privilegeRequest = new PrivilegeRequest();
        privilegeRequest.setCatalogs(grantReq.getCatalogs());
        privilegeRequest.setDatabases(grantReq.getDatabases());
        privilegeRequest.setTables(grantReq.getTables());
        for(TablePrivilege tp:grantReq.getTables()){
            tp.getKey().setCatalog("internal");
        }
        return  privilegeRequest;
    }
}

package com.gitee.dbswitch.admin.model.response;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ApiModel("流程处理结果")
public class ProcResultResp {
    private boolean status;
    private String msg;
}

package com.gitee.dbswitch.admin.dao;

import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.gitee.dbswitch.admin.entity.ProcessInfoEntity;
import com.gitee.dbswitch.admin.mapper.ProcessMapper;
import com.gitee.dbswitch.admin.mapper.TableInfoMapper;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

@Repository
public class ProcessDao {
    @Resource
    private ProcessMapper processMapper;
    @Resource
    private TableInfoMapper tableInfoMapper;

    public void insert(ProcessInfoEntity processInfoEntity) {
        processMapper.insert(processInfoEntity);
    }

    public ProcessInfoEntity getById(Long id) {
        return processMapper.selectById(id);
    }

    public List<ProcessInfoEntity> listAll(String searchText,Integer status,String username) {

        //ProcessInfoEntity processInfoEntity = processMapper.selectById(6);
        //return Arrays.asList(processInfoEntity);
        return processMapper.selectList(
                Wrappers.<ProcessInfoEntity>lambdaQuery()
                        .like(StringUtils.isNotBlank(searchText),ProcessInfoEntity::getProcessInfoJson, searchText)
                        .eq(ObjectUtils.isNotEmpty(status),ProcessInfoEntity::getProcessStatus,status)
                        .eq(StringUtils.isNotBlank(username),ProcessInfoEntity::getUsername,username)
                        .orderByDesc(ProcessInfoEntity::getCreateTime)
        );
    }

    public void updateById(ProcessInfoEntity processInfoEntity) {
        processMapper.updateById(processInfoEntity);
    }

    public void deleteById(Long id) {
        processMapper.deleteById(id);
    }

    public int getTotalCount() {
        return processMapper.selectCount(null).intValue();
    }




}

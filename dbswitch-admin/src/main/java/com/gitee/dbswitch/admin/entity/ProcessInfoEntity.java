package com.gitee.dbswitch.admin.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.gitee.dbswitch.common.type.ProcessTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.ibatis.type.EnumTypeHandler;

import java.sql.Timestamp;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@TableName(value ="DBSWITCH_PROCESS_INFO", autoResultMap = true)
public class ProcessInfoEntity {
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @TableField(value = "database_id")
    private Long databaseId;

    @TableField(value = "type")
    private String processtype;

    @TableField(value ="username")
    private String username;

    @TableField(value ="process_info_json")
    private String processInfoJson;

    @TableField(value ="process_status")
    private Integer processStatus;

    @TableField(value ="remarks")
    private String remarks;

    @TableField(value = "create_time", insertStrategy = FieldStrategy.NEVER, updateStrategy = FieldStrategy.NEVER)
    private Timestamp createTime;

    @TableField(value = "update_time", insertStrategy = FieldStrategy.NEVER, updateStrategy = FieldStrategy.NEVER)
    private Timestamp updateTime;

}

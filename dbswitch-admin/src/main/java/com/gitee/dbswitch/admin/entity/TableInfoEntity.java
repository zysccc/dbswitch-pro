package com.gitee.dbswitch.admin.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.gitee.dbswitch.common.type.ProcessTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.ibatis.type.EnumTypeHandler;

import java.sql.Timestamp;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@TableName(value ="zys_table_manage",autoResultMap = true)
public class TableInfoEntity {
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @TableField(value = "database_id")
    private Long databaseId;

    @TableField(value = "database_name")
    private String databaseName;

    @TableField(value ="dbname")
    private String dbname;

    @TableField(value ="table_name")
    private String tableName;

    @TableField(value ="itrp")
    private String itrp;

    @TableField(value ="itrp_dept")
    private String itrpDept;

    @TableField(value ="owner")
    private String owner;

    @TableField(value ="owner_dept")
    private String ownerDept;

    @TableField(value ="partition_type")
    private String partition;

    @TableField(value ="est_total_rows")
    private Long estTotalRows;

    @TableField(value ="est_daily_rows")
    private Long estDailyRows;

    @TableField(value ="data_freq")
    private String dataFreq;

    @TableField(value ="data_level")
    private String dataLevel;

    @TableField(value ="data_desc")
    private String dataDesc;

    @TableField(value ="ddl")
    private String ddl;

    @TableField(value = "create_time", insertStrategy = FieldStrategy.NEVER, updateStrategy = FieldStrategy.NEVER)
    private Timestamp createTime;

    @TableField(value = "update_time", insertStrategy = FieldStrategy.NEVER, updateStrategy = FieldStrategy.NEVER)
    private Timestamp updateTime;
}

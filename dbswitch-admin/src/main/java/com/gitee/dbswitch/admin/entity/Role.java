package com.gitee.dbswitch.admin.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@TableName("sys_role")
public class Role {
    /**
     * 表：sys_role
     * 字段：id
     * 注释：
     *
     * @mbggenerated
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 表：sys_role
     * 字段：name
     * 注释：角色
     *
     * @mbggenerated
     */
    @TableField("name")
    private String name;

    /**
     * 表：sys_role
     * 字段：remark
     * 注释：备注
     *
     * @mbggenerated
     */
    @TableField("remark")
    private String remark;

}
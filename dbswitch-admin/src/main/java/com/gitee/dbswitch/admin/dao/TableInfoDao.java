package com.gitee.dbswitch.admin.dao;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.gitee.dbswitch.admin.entity.DatabaseConnectionEntity;
import com.gitee.dbswitch.admin.entity.ProcessInfoEntity;
import com.gitee.dbswitch.admin.entity.TableInfoEntity;
import com.gitee.dbswitch.admin.mapper.ProcessMapper;
import com.gitee.dbswitch.admin.mapper.TableInfoMapper;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

@Repository
public class TableInfoDao {
    @Resource
    private TableInfoMapper tableInfoMapper;

    public void insert(TableInfoEntity tableInfoEntity) {
        tableInfoMapper.insert(tableInfoEntity);
    }

    public TableInfoEntity getById(Long id) {
        return tableInfoMapper.selectById(id);
    }

    public List<TableInfoEntity> listAll(String tablename,String owner,String itrp,Long dbid ) {

        return tableInfoMapper.selectList(
                Wrappers.<TableInfoEntity>lambdaQuery()
                        .like(StringUtils.hasText(tablename),TableInfoEntity::getTableName, tablename)
                        .like(StringUtils.hasText(owner),TableInfoEntity::getOwner, owner)
                        .like(StringUtils.hasText(itrp),TableInfoEntity::getItrp, itrp)
                        .eq(Objects.nonNull(dbid),TableInfoEntity::getDatabaseId,dbid)
                        .orderByDesc(TableInfoEntity::getCreateTime)
        );
    }
    public void updateById(TableInfoEntity tableInfoEntity) {
        tableInfoMapper.updateById(tableInfoEntity);
    }

    public void deleteById(Long id) {
        tableInfoMapper.deleteById(id);
    }

    public int getTotalCount() {
        return tableInfoMapper.selectCount(null).intValue();
    }



}

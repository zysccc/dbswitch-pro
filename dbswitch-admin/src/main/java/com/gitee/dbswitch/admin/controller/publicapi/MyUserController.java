package com.gitee.dbswitch.admin.controller.publicapi;

import com.gitee.dbswitch.admin.entity.User;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/user")
public class MyUserController {


    @GetMapping("/none")
    public String test(HttpServletRequest request) {
        return "这个接口不拦截";
    }

    /**
     * 获取用户
     * @param request
     * @return
     */
    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('auth:user')")
    public User getUser(HttpServletRequest request, @PathVariable Integer id) {
        return null;
    }

    /**
     * 获取登陆人
     * @return
     */
    @GetMapping("/loginUser")
    public String getLoginUser() {
        return "";
    }


}

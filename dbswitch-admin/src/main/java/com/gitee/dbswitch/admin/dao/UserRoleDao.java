package com.gitee.dbswitch.admin.dao;

import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.gitee.dbswitch.admin.entity.UserRole;
import com.gitee.dbswitch.admin.mapper.UserRoleMapper;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

@Repository
public class UserRoleDao {

    @Resource
    private UserRoleMapper userRoleMapper;

    public UserRole selectByUserId(Integer userId){
        return userRoleMapper.selectOne(Wrappers.<UserRole>lambdaQuery().eq(ObjectUtils.isNotNull(userId),UserRole::getUserId,userId));
    }

    public int deleteByPrimaryKey(Integer id){
        return userRoleMapper.deleteById(id);
    }

    public int insertSelective(UserRole record){
        return userRoleMapper.insert(record);
    }

    public UserRole selectByPrimaryKey(Integer id){
        return userRoleMapper.selectById(id);
    }

    public int updateByPrimaryKeySelective(UserRole record){
        return userRoleMapper.updateById(record);
    }
}

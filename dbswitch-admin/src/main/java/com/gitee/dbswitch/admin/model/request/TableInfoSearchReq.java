package com.gitee.dbswitch.admin.model.request;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class TableInfoSearchReq {
    private String searchtablename;
    private Long databaseId;
    private String itrp;
    private String itrpDept;
    private String owner;
    private String ownerDept;
    private Integer page;
    private Integer size;
}

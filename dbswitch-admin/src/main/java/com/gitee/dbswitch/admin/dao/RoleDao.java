package com.gitee.dbswitch.admin.dao;

import com.gitee.dbswitch.admin.entity.Role;
import com.gitee.dbswitch.admin.mapper.RoleMapper;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

@Repository
public class RoleDao {
    @Resource
    private RoleMapper roleMapper;

    public  int deleteByPrimaryKey(Integer id){
            return roleMapper.deleteById(id);
    }

    public int insertSelective(Role record){
        return roleMapper.insert(record);
    }

    public Role selectByPrimaryKey(Integer id){
        return roleMapper.selectById(id);    }

    public int updateByPrimaryKeySelective(Role record){
        return roleMapper.updateById(record);
    }
}
